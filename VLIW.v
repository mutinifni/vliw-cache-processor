//adder

module adder(input [31:0] in1, input [31:0] in2, output reg [31:0] adder_out);

	always@(in1 or in2)
	
		adder_out = in1 +in2;
		
endmodule

//alu
module alu(input [31:0] aluIn1, input [31:0] aluIn2, input [1:0] aluOp, output reg [31:0] aluOut, output reg ZWO, output reg NWO, output reg CWO, output reg VWO );

	always@(aluIn1 or aluIn2 or aluOp)
	begin
	
		CWO = 0;
		VWO = 0;
		case(aluOp)
		
		
			2'b00: 
			begin
				
				{CWO,aluOut} = aluIn1 + aluIn2;
				NWO = aluOut[31];
				if(aluOut == 0)		ZWO = 1;
				else 	ZWO = 0;
				if((aluIn1[31] == 1'b1 && aluIn2[31] == 1'b1 && aluOut[31] == 1'b0) || (aluIn1[31] == 1'b0 && aluIn2[31] == 1'b0 && aluOut[31] == 1'b1))
				  VWO = 1;
				else	
				  VWO = 0;
			end
			
			2'b01: 
			begin
				
				{CWO,aluOut} = aluIn1 - aluIn2;
				NWO = aluOut[31];
				
				if(aluOut == 0)		ZWO = 1;
				else	ZWO = 0;
			
				if((aluIn1[31] == 1'b0 && aluIn2[31] == 1'b1 && aluOut[31] == 1'b1) || (aluIn1[31] == 1'b1 && aluIn2[31] == 1'b0 && aluOut[31] == 1'b0))
				  VWO = 1;
				else	
				  VWO = 0;
				
			end
			
			2'b10: 
			begin
				
				{CWO,aluOut} = aluIn2 - aluIn1;
				NWO = aluOut[31];
				
				if(aluOut == 0)		ZWO = 1;
				else	ZWO = 0;
				
				if((aluIn1[31] == 1'b1 && aluIn2[31] == 1'b0 && aluOut[31] == 1'b1) || (aluIn1[31] == 1'b0 && aluIn2[31] == 1'b1 && aluOut[31] == 1'b0))
				  VWO = 1;
				else	
				  VWO = 0;
			
			end
			
			2'b11: 
			begin
				
				aluOut = aluIn1 ^ aluIn2;
				NWO = aluOut[31];
				
				if(aluOut == 0)		ZWO = 1;
				else	ZWO = 0;
			
			end
		endcase
	end
endmodule


//control_circuit

module ctrlCkt	( input [4:0] opcode1, input [4:0] opcode2, input cmp_or_xor, input is_exception, output reg memReadI2,  output reg [1:0] aluOpI1, output reg memWriteI2,  output reg regWriteI1, output reg regWriteI2, output reg undef, output reg f1, output reg f2, output reg [1:0] PCSrc );
reg branchI2, jmpI2;
always @(opcode1,opcode2,cmp_or_xor, is_exception)
begin	
f1 = 1'b0;
f2 = 1'b0;
case(opcode2)
        5'b00000:begin //nop
					branchI2=1'b0;
					jmpI2=1'b0;
					memReadI2= 1'b0;
					memWriteI2=1'b0;
					regWriteI2=1'b0;
					undef=1'b0;
					
        end
				5'b01010:begin		//load
					branchI2=1'b0;
					jmpI2=1'b0;
					memReadI2= 1'b1;
					memWriteI2=1'b0;
					regWriteI2=1'b1;
					undef=1'b0;
					f2 = 1'b1;
				end
				
				5'b01011:begin		//store
					branchI2=1'b0;
					jmpI2=1'b0;
					memReadI2= 1'b0;
					memWriteI2=1'b1;
					regWriteI2=1'b0;
					undef=1'b0;
				end
				
				5'b11110:begin		//jump
					branchI2=1'b0;
					jmpI2=1'b1;
					memReadI2= 1'b0;
					memWriteI2=1'b0;
					regWriteI2=1'b0;
					undef=1'b0;
				end
				
				5'b11010:begin 		//branch
					branchI2=1'b1;
					jmpI2=1'b0;
					memReadI2= 1'b0;
					memWriteI2=1'b0;
					regWriteI2=1'b0;
					undef=1'b0;
				end
				default:begin //undef
					branchI2=1'b0;
					jmpI2=1'b0;
					memReadI2= 1'b0;
					memWriteI2=1'b0;
					regWriteI2=1'b0;
					undef=1'b1;
				end
				
endcase

case(opcode1)
  5'b00000:begin //nop
			aluOpI1=2'b00;
			regWriteI1=1'b0;
			undef=1'b0;
  end
  
	5'b00100:begin		//add
			aluOpI1=2'b00;
			regWriteI1=1'b1;
			undef=1'b0;
			f1 = 1'b1;
	end
	
	5'b00011:begin		//subtract
					aluOpI1=2'b01;
					regWriteI1=1'b1;
					undef=1'b0;
					f1 = 1'b1;
	end
	
	5'b01000:begin		//compare
		if(cmp_or_xor==0)
		  begin
					aluOpI1=2'b10;
					regWriteI1=1'b0;
					undef=1'b0;
					f1 = 1'b1;
		  end
		else  		//same opcode as compare for xor
		  begin
					aluOpI1=2'b11;
					regWriteI1=1'b1;
					undef=1'b0;
					f2 = 1'b1;
		  end
		end
		default:begin //undef
			aluOpI1=2'b00;
			regWriteI1=1'b0;
			undef=1'b1;
		end
	endcase
	
PCSrc = 2'b00;	
if(is_exception) PCSrc = 2'b10;
else
begin
	if(jmpI2) PCSrc = 2'b11;
	if(branchI2) PCSrc = 2'b01;
end
end
endmodule


//modified_D_ff

module D_ff (input clk, input reset, input regWrite1, input regWrite2, input decOut1b1, input decOut1b2, input d1, input d2, output reg q);
	always @ (posedge clk)
	begin
	if(reset==1'b1)
		q=0;
	else
		begin
		if(regWrite1 == 1'b1 && decOut1b1==1'b1) q=d1; 
		if(regWrite2 == 1'b1 && decOut1b2==1'b1) q=d2;
		end
	end
endmodule

//decoder3to8

module decoder3to8( input [2:0] destReg, output reg [7:0] decOut);
	always@(destReg)
	case(destReg)
			3'b000: decOut=8'b00000001; 
			3'b001: decOut=8'b00000010;
			3'b010: decOut=8'b00000100;
			3'b011: decOut=8'b00001000;
			3'b100: decOut=8'b00010000;
			3'b101: decOut=8'b00100000;
			3'b110: decOut=8'b01000000;
			3'b111: decOut=8'b10000000;
			endcase
endmodule

//normal_D_ff

module D_ff_p (input clk, input reset, input regWrite, input decOut1b, input d, output reg q);
	always @ (negedge clk)
	begin
	if(reset==1'b1)
		q=0;
	else
		if(regWrite == 1'b1 && decOut1b==1'b1) begin q=d; end
	end
endmodule

//register32bit

module register32bit_p( input clk, input reset, input regWrite, input decOut1b, input [31:0] writeData, output  [31:0] outR );
	D_ff_p d0(clk, reset, regWrite, decOut1b, writeData[0], outR[0]);
	D_ff_p d1(clk, reset, regWrite, decOut1b, writeData[1], outR[1]);
	D_ff_p d2(clk, reset, regWrite, decOut1b, writeData[2], outR[2]);
	D_ff_p d3(clk, reset, regWrite, decOut1b, writeData[3], outR[3]);
	D_ff_p d4(clk, reset, regWrite, decOut1b, writeData[4], outR[4]);
	D_ff_p d5(clk, reset, regWrite, decOut1b, writeData[5], outR[5]);
	D_ff_p d6(clk, reset, regWrite, decOut1b, writeData[6], outR[6]);
	D_ff_p d7(clk, reset, regWrite, decOut1b, writeData[7], outR[7]);
	D_ff_p d8(clk, reset, regWrite, decOut1b, writeData[8], outR[8]);
	D_ff_p d9(clk, reset, regWrite, decOut1b, writeData[9], outR[9]);
	D_ff_p d10(clk, reset, regWrite, decOut1b, writeData[10], outR[10]);
	D_ff_p d11(clk, reset, regWrite, decOut1b, writeData[11], outR[11]);
	D_ff_p d12(clk, reset, regWrite, decOut1b, writeData[12], outR[12]);
	D_ff_p d13(clk, reset, regWrite, decOut1b, writeData[13], outR[13]);
	D_ff_p d14(clk, reset, regWrite, decOut1b, writeData[14], outR[14]);
	D_ff_p d15(clk, reset, regWrite, decOut1b, writeData[15], outR[15]);
	D_ff_p d16(clk, reset, regWrite, decOut1b, writeData[16], outR[16]);
	D_ff_p d17(clk, reset, regWrite, decOut1b, writeData[17], outR[17]);
	D_ff_p d18(clk, reset, regWrite, decOut1b, writeData[18], outR[18]);
	D_ff_p d19(clk, reset, regWrite, decOut1b, writeData[19], outR[19]);
	D_ff_p d20(clk, reset, regWrite, decOut1b, writeData[20], outR[20]);
	D_ff_p d21(clk, reset, regWrite, decOut1b, writeData[21], outR[21]);
	D_ff_p d22(clk, reset, regWrite, decOut1b, writeData[22], outR[22]);
	D_ff_p d23(clk, reset, regWrite, decOut1b, writeData[23], outR[23]);
	D_ff_p d24(clk, reset, regWrite, decOut1b, writeData[24], outR[24]);
	D_ff_p d25(clk, reset, regWrite, decOut1b, writeData[25], outR[25]);
	D_ff_p d26(clk, reset, regWrite, decOut1b, writeData[26], outR[26]);
	D_ff_p d27(clk, reset, regWrite, decOut1b, writeData[27], outR[27]);
	D_ff_p d28(clk, reset, regWrite, decOut1b, writeData[28], outR[28]);
	D_ff_p d29(clk, reset, regWrite, decOut1b, writeData[29], outR[29]);
	D_ff_p d30(clk, reset, regWrite, decOut1b, writeData[30], outR[30]);
	D_ff_p d31(clk, reset, regWrite, decOut1b, writeData[31], outR[31]);
endmodule

//register16bit

module register16bit_p( input clk, input reset, input regWrite, input decOut1b, input [15:0] writeData, output  [15:0] outR );
	D_ff_p d0(clk, reset, regWrite, decOut1b, writeData[0], outR[0]);
	D_ff_p d1(clk, reset, regWrite, decOut1b, writeData[1], outR[1]);
	D_ff_p d2(clk, reset, regWrite, decOut1b, writeData[2], outR[2]);
	D_ff_p d3(clk, reset, regWrite, decOut1b, writeData[3], outR[3]);
	D_ff_p d4(clk, reset, regWrite, decOut1b, writeData[4], outR[4]);
	D_ff_p d5(clk, reset, regWrite, decOut1b, writeData[5], outR[5]);
	D_ff_p d6(clk, reset, regWrite, decOut1b, writeData[6], outR[6]);
	D_ff_p d7(clk, reset, regWrite, decOut1b, writeData[7], outR[7]);
	D_ff_p d8(clk, reset, regWrite, decOut1b, writeData[8], outR[8]);
	D_ff_p d9(clk, reset, regWrite, decOut1b, writeData[9], outR[9]);
	D_ff_p d10(clk, reset, regWrite, decOut1b, writeData[10], outR[10]);
	D_ff_p d11(clk, reset, regWrite, decOut1b, writeData[11], outR[11]);
	D_ff_p d12(clk, reset, regWrite, decOut1b, writeData[12], outR[12]);
	D_ff_p d13(clk, reset, regWrite, decOut1b, writeData[13], outR[13]);
	D_ff_p d14(clk, reset, regWrite, decOut1b, writeData[14], outR[14]);
	D_ff_p d15(clk, reset, regWrite, decOut1b, writeData[15], outR[15]);
endmodule


//register8bit

module register8bit_p( input clk, input reset, input regWrite, input decOut1b, input [7:0] writeData, output  [7:0] outR );
	D_ff_p d0(clk, reset, regWrite, decOut1b, writeData[0], outR[0]);
	D_ff_p d1(clk, reset, regWrite, decOut1b, writeData[1], outR[1]);
	D_ff_p d2(clk, reset, regWrite, decOut1b, writeData[2], outR[2]);
	D_ff_p d3(clk, reset, regWrite, decOut1b, writeData[3], outR[3]);
	D_ff_p d4(clk, reset, regWrite, decOut1b, writeData[4], outR[4]);
	D_ff_p d5(clk, reset, regWrite, decOut1b, writeData[5], outR[5]);
	D_ff_p d6(clk, reset, regWrite, decOut1b, writeData[6], outR[6]);
	D_ff_p d7(clk, reset, regWrite, decOut1b, writeData[7], outR[7]);
	
endmodule

//register3bit

module register3bit_p( input clk, input reset, input regWrite, input decOut1b, input [2:0] writeData, output  [2:0] outR );
	D_ff_p d0(clk, reset, regWrite, decOut1b, writeData[0], outR[0]);
	D_ff_p d1(clk, reset, regWrite, decOut1b, writeData[1], outR[1]);
	D_ff_p d2(clk, reset, regWrite, decOut1b, writeData[2], outR[2]);
	
endmodule

//register1bit

module register1bit_p( input clk, input reset, input regWrite, input decOut1b, input  writeData, output  outR );
	D_ff_p d0(clk, reset, regWrite, decOut1b, writeData, outR);
endmodule

//register2bit

module register2bit_p( input clk, input reset, input regWrite, input decOut1b, input [1:0] writeData, output [1:0] outR );
	D_ff_p d0(clk, reset, regWrite, decOut1b, writeData[0], outR[0]);
	D_ff_p d1(clk, reset, regWrite, decOut1b, writeData[1], outR[1]);
endmodule

//IF_ID_pipeline

module IF_ID(input clk, input reset,input IFIDregWrite, input decOut1b, input [15:0] instr1, input [15:0] instr2,
input [31:0] pcplusfour, output [15:0] p0_intr1, output [15:0] p0_intr2, output [31:0] p0_pcplusfour);
	
	//not sure if 2 decout1b are needed, only 1 is used, 16 bit pc used
	//register16bit r1 (clk, reset, regWrite, decOut[1] , writeData , outR1 );
register16bit_p firstinst (clk, reset, IFIDregWrite, decOut1b , instr1 ,  p0_intr1 );
register16bit_p secondinst (clk, reset, IFIDregWrite, decOut1b , instr2 ,  p0_intr2 );
register32bit_p pcfour (clk, reset, IFIDregWrite, decOut1b , pcplusfour ,  p0_pcplusfour );

endmodule

//ID_EX_pipeline

module ID_EX(input clk, input reset,input IDEXWrite, input decOut1b,input [31:0] p0_pc, input [31:0] regOut1I1,input [31:0] regOut2I1,
  input [31:0] regOut1I2,input [31:0] regOut2I2, input [31:0] regOut3I2,
input [2:0] inst1_Rs,input [2:0] inst1_Rt, input [2:0] inst1_Rdmuxed, input [2:0] inst2_Rs,input [2:0] inst2_Rt, input [2:0] inst2_Rd, input [7:0] Inst1_immediate,
input [1:0] ctr_aluOpI1, input cause_invalid_intr, input ctr_memRead, input ctr_memWrite,input ctr_regWrite1, input ctr_regWrite2, input ctr_is_add, input f1, input f2,
  output [31:0] p1_pc, output [31:0] p1_regOut1I1,output [31:0] p1_regOut2I1, output [31:0] p1_regOut1I2, output [31:0] p1_regOut2I2,  output [31:0] p1_regOut3I2,     
  output [2:0] p1_inst1_Rs, output [2:0] p1_inst1_Rt, output [2:0] p1_inst1_Rdmuxed, output [2:0] p1_inst2_Rs, output [2:0] p1_inst2_Rt, output [2:0] p1_inst2_Rd,
  output [7:0] p1_Inst1_immediate,
  output [1:0] p1_aluOpI1,  output  p1_memRead,
  output  p1_memWrite,output  p1_regWrite1, output  p1_regWrite2,
 output p1_invalid_intr, output p1_is_add, output p1_f1, output p1_f2);
//not using pi_toreg assuming it is memtoreg, regdst need not be fwded, but still am forwarding, remove if unncessary
//not using pi_toreg assuming it is memtoreg, regdst need not be fwded, but still am forwarding, remove if unncessary
register32bit_p out11 (clk, reset, IDEXWrite, decOut1b , regOut1I1 , p1_regOut1I1 );
register32bit_p out12 (clk, reset, IDEXWrite, decOut1b , regOut2I1 , p1_regOut2I1 );
register32bit_p out21 (clk, reset, IDEXWrite, decOut1b , regOut1I2 , p1_regOut1I2 );
register32bit_p out22 (clk, reset, IDEXWrite, decOut1b , regOut2I2 , p1_regOut2I2 );
register32bit_p out23 (clk, reset, IDEXWrite, decOut1b , regOut3I2 , p1_regOut3I2);

// register4bit( input clk, input reset, input regWrite, input decOut1b, input [3:0] writeData, output  [3:0] outR );
register3bit_p   rsi1(  clk, reset,  IDEXWrite,  decOut1b,  inst1_Rs,      p1_inst1_Rs );
register3bit_p   rti1(  clk, reset,  IDEXWrite,  decOut1b,  inst1_Rt,      p1_inst1_Rt );
register3bit_p   rdi1(  clk, reset,  IDEXWrite,  decOut1b,  inst1_Rdmuxed, p1_inst1_Rdmuxed );
register3bit_p   rsi2(  clk, reset,  IDEXWrite,  decOut1b,  inst2_Rs, p1_inst2_Rs );
register3bit_p   rti2(  clk, reset,  IDEXWrite,  decOut1b,  inst2_Rt, p1_inst2_Rt );
register3bit_p   rdi2(  clk, reset,  IDEXWrite,  decOut1b,  inst2_Rd, p1_inst2_Rd );
//rt rd previously??

	register8bit_p   immvalue(  clk, reset,  IDEXWrite,  decOut1b,  Inst1_immediate,      p1_Inst1_immediate );


//register 16 bit:
	register32bit_p   pcvalue(  clk, reset,  IDEXWrite,  decOut1b,  p0_pc, p1_pc);



//Register 1 bit
//module register1bit( input clk, input reset, input regWrite, input decOut1b, input writeData, output outR );
register1bit_p  r2( clk, reset,  IDEXWrite,  decOut1b, ctr_regWrite1,p1_regWrite1 );
register1bit_p  r5( clk, reset,  IDEXWrite,  decOut1b, ctr_memRead, p1_memRead );
register1bit_p  r6( clk, reset,  IDEXWrite,  decOut1b, ctr_memWrite,p1_memWrite );
register1bit_p  r7( clk, reset,  IDEXWrite,  decOut1b, ctr_regWrite2,p1_regWrite2 );
register1bit_p  r8( clk, reset,  IDEXWrite,  decOut1b, cause_invalid_intr,  p1_invalid_intr);
register1bit_p  r10( clk, reset,  IDEXWrite,  decOut1b, ctr_is_add,  p1_is_add);
	

//Register 2 bits

register2bit_p  r9(  clk,  reset, IDEXWrite,  decOut1b, ctr_aluOpI1, p1_aluOpI1 );
register1bit_p  r11(  clk,  reset, IDEXWrite,  decOut1b, f1, p1_f1 );
register1bit_p  r12(  clk,  reset, IDEXWrite,  decOut1b, f2, p1_f2 );
endmodule

//EX_MEM_pipeline

module EX_MEM(input clk, input reset,input EXMemWrite, input decOut1b,
   input [31:0] aluOut,  input [31:0] adderOut, input p1_memRead, input p1_memWrite,input p1_regWrite1,
		input p1_regWrite2, input [2:0] RdI1, input [2:0] RdI2, input [31:0] p1_regRdI2,
input p1_zflag, input p1_nflag, input p1_cflag,
		input p1_vflag, input p1_f1, input p1_f2,
		output [31:0] p2_aluOut, output [31:0] p2_adderOut,
	output  p2_memRead, output p2_memWrite,	 
	output  p2_regWrite1, output  p2_regWrite2,  output [2:0] p2_RdI1, output [31:0] p2_regRdI2, output [2:0] p2_RdI2, output p2_zflag, output p2_nflag, output p2_cflag, output p2_vflag, output p2_f1, output p2_f2 );
	

//32 bit registers
register32bit_p alout(clk, reset, EXMemWrite, decOut1b , aluOut , p2_aluOut );
register32bit_p adderout(clk, reset, EXMemWrite, decOut1b , adderOut , p2_adderOut );
register32bit_p rdi2 (clk, reset, EXMemWrite, decOut1b , p1_regRdI2 , p2_regRdI2 );

//Register 3 bit
register3bit_p  r31( clk, reset,  EXMemWrite,  decOut1b, RdI1,  p2_RdI1 );
register3bit_p  r32( clk, reset,  EXMemWrite,  decOut1b, RdI2,  p2_RdI2 );
//check right to left flow...

//Register 1 bit

register1bit_p  r1( clk, reset,  EXMemWrite,  decOut1b, p1_memRead,  p2_memRead );
register1bit_p  r2( clk, reset,  EXMemWrite,  decOut1b, p1_memWrite, p2_memWrite );
register1bit_p  r3( clk, reset,  EXMemWrite,  decOut1b, p1_regWrite1, p2_regWrite1 );
register1bit_p  r4( clk, reset,  EXMemWrite,  decOut1b, p1_regWrite2, p2_regWrite2 );
register1bit_p  r5( clk, reset,  EXMemWrite,  decOut1b, p1_zflag,  p2_zflag );
register1bit_p  r6( clk, reset,  EXMemWrite,  decOut1b, p1_nflag,  p2_nflag );
register1bit_p  r7( clk, reset,  EXMemWrite,  decOut1b, p1_cflag,  p2_cflag );
register1bit_p  r8( clk, reset,  EXMemWrite,  decOut1b, p1_vflag,  p2_vflag );
register1bit_p  r11(  clk,  reset, EXMemWrite,  decOut1b, p1_f1, p2_f1 );
register1bit_p  r12(  clk,  reset, EXMemWrite,  decOut1b, p1_f2, p2_f2 );

endmodule

//MEM_WB_pipeline

module MEM_WB(input clk, input reset,input MemWbWrite, input decOut1b, input [31:0] p2_aluOut,
input [31:0] memOut,input p2_regWrite1, input p2_regWrite2,
//right ot left control flow
 input [2:0] RdI1MemWb, input [2:0] RdI2MemWb,
input p2_zflag, input p2_nflag, input p2_cflag,
		input p2_vflag,input p2_f1, input p2_f2,


	output [31:0] p3_aluOut, output [2:0] p3_RdI1MemWb, output [2:0] p3_RdI2MemWb,
	output [31:0] p3_memOut, output  p3_regWrite1, output  p3_regWrite2, output p3_zflag, output p3_nflag, output p3_cflag, output p3_vflag, output p3_f1, output p3_f2 );


register32bit_p sign (clk, reset, MemWbWrite, decOut1b , p2_aluOut , p3_aluOut );
register32bit_p si (clk, reset, MemWbWrite, decOut1b , memOut , p3_memOut );


//Register 1 bit

register1bit_p  r1( clk, reset,  MemWbWrite,  decOut1b, p2_regWrite1,  p3_regWrite1);
register1bit_p  r2( clk, reset,  MemWbWrite,  decOut1b, p2_regWrite2,  p3_regWrite2);
register1bit_p  r5( clk, reset,  MemWbWrite,  decOut1b, p2_zflag,  p3_zflag );
register1bit_p  r6( clk, reset,  MemWbWrite,  decOut1b, p2_nflag,  p3_nflag );
register1bit_p  r7( clk, reset,  MemWbWrite,  decOut1b, p2_cflag,  p3_cflag );
register1bit_p  r8( clk, reset,  MemWbWrite,  decOut1b, p2_vflag,  p3_vflag );

//Register 3 bits
register3bit_p  r3( clk, reset,  MemWbWrite,  decOut1b, RdI1MemWb,  p3_RdI1MemWb);
register3bit_p  r4( clk, reset,  MemWbWrite,  decOut1b, RdI2MemWb,  p3_RdI2MemWb);

register1bit_p  r11(  clk,  reset, MemWbWrite,  decOut1b, p2_f1, p3_f1 );
register1bit_p  r12(  clk,  reset, MemWbWrite,  decOut1b, p2_f2, p3_f2 );

endmodule

//D_ff_Mem

module D_ff_Mem (input clk, input reset, input regWrite, input decOut1b,input init, input d, output reg q);
	always @ (negedge clk)
	begin
	if(reset==1)
		q=init;
	else
		if(regWrite == 1 && decOut1b==1) begin q=d; end
	end
endmodule

//register_Mem

module register_Mem(input clk,input reset,input regWrite,input decOut1b,input [31:0]init, input [31:0] d_in, output [31:0] q_out);
	D_ff_Mem dMem0 (clk,reset,regWrite,decOut1b,init[0],d_in[0],q_out[0]);
	D_ff_Mem dMem1 (clk,reset,regWrite,decOut1b,init[1],d_in[1],q_out[1]);
	D_ff_Mem dMem2 (clk,reset,regWrite,decOut1b,init[2],d_in[2],q_out[2]);
	D_ff_Mem dMem3 (clk,reset,regWrite,decOut1b,init[3],d_in[3],q_out[3]);
	
	D_ff_Mem dMem4 (clk,reset,regWrite,decOut1b,init[4],d_in[4],q_out[4]);
	D_ff_Mem dMem5 (clk,reset,regWrite,decOut1b,init[5],d_in[5],q_out[5]);
	D_ff_Mem dMem6 (clk,reset,regWrite,decOut1b,init[6],d_in[6],q_out[6]);
	D_ff_Mem dMem7 (clk,reset,regWrite,decOut1b,init[7],d_in[7],q_out[7]);

	D_ff_Mem dMem8 (clk,reset,regWrite,decOut1b,init[8],d_in[8],q_out[8]);
	D_ff_Mem dMem9 (clk,reset,regWrite,decOut1b,init[9],d_in[9],q_out[9]);
	D_ff_Mem dMem10 (clk,reset,regWrite,decOut1b,init[10],d_in[10],q_out[10]);
	D_ff_Mem dMem11 (clk,reset,regWrite,decOut1b,init[11],d_in[11],q_out[11]);
	
	D_ff_Mem dMem12 (clk,reset,regWrite,decOut1b,init[12],d_in[12],q_out[12]);
	D_ff_Mem dMem13 (clk,reset,regWrite,decOut1b,init[13],d_in[13],q_out[13]);
	D_ff_Mem dMem14 (clk,reset,regWrite,decOut1b,init[14],d_in[14],q_out[14]);
	D_ff_Mem dMem15 (clk,reset,regWrite,decOut1b,init[15],d_in[15],q_out[15]);

	D_ff_Mem dMem16 (clk,reset,regWrite,decOut1b,init[16],d_in[16],q_out[16]);
	D_ff_Mem dMem17 (clk,reset,regWrite,decOut1b,init[17],d_in[17],q_out[17]);
	D_ff_Mem dMem18 (clk,reset,regWrite,decOut1b,init[18],d_in[18],q_out[18]);
	D_ff_Mem dMem19 (clk,reset,regWrite,decOut1b,init[19],d_in[19],q_out[19]);
	
	D_ff_Mem dMem20 (clk,reset,regWrite,decOut1b,init[20],d_in[20],q_out[20]);
	D_ff_Mem dMem21 (clk,reset,regWrite,decOut1b,init[21],d_in[21],q_out[21]);
	D_ff_Mem dMem22 (clk,reset,regWrite,decOut1b,init[22],d_in[22],q_out[22]);
	D_ff_Mem dMem23 (clk,reset,regWrite,decOut1b,init[23],d_in[23],q_out[23]);

	D_ff_Mem dMem24 (clk,reset,regWrite,decOut1b,init[24],d_in[24],q_out[24]);
	D_ff_Mem dMem25 (clk,reset,regWrite,decOut1b,init[25],d_in[25],q_out[25]);
	D_ff_Mem dMem26 (clk,reset,regWrite,decOut1b,init[26],d_in[26],q_out[26]);
	D_ff_Mem dMem27 (clk,reset,regWrite,decOut1b,init[27],d_in[27],q_out[27]);
	
	D_ff_Mem dMem28 (clk,reset,regWrite,decOut1b,init[28],d_in[28],q_out[28]);
	D_ff_Mem dMem29 (clk,reset,regWrite,decOut1b,init[29],d_in[29],q_out[29]);
	D_ff_Mem dMem30 (clk,reset,regWrite,decOut1b,init[30],d_in[30],q_out[30]);
	D_ff_Mem dMem31 (clk,reset,regWrite,decOut1b,init[31],d_in[31],q_out[31]);
	
endmodule

//Mem

module Mem(input clk, input reset,input memWrite,input memRead, input [31:0] pc, input [31:0] dataIn,output [15:0] IR1, [15:0] IR2 );
	wire [31:0] Qout0, Qout1, Qout2, Qout3, Qout4, Qout5, Qout6, Qout7,
					Qout8, Qout9, Qout10, Qout11, Qout12, Qout13, Qout14, Qout15;
	wire [15:0] decOut;
	wire [31:0] IR;
	decoder4to16 dec0( pc[5:2], decOut);
	
	register_Mem r0(clk,reset,memWrite,decOut[0],32'b 00100_000_00000100_01010_00_001_001_001,dataIn,Qout0);  //add, jmp
	register_Mem r1(clk,reset,memWrite,decOut[1],32'b 00011_01_000_001_011_01010_00_000_101_101,dataIn,Qout1);  //add, store
	register_Mem r2(clk,reset,memWrite,decOut[2],32'b 00011_01_101_001_101_00000_00_000_000_000,dataIn,Qout2);  //add, load
	register_Mem r3(clk,reset,memWrite,decOut[3],32'b 00100_000_00000100_00000_00_000_000_000,dataIn,Qout3); //sub, store
	
	register_Mem r4(clk,reset,memWrite,decOut[4],32'b 00100_000_00000100_0000000000000000,dataIn,Qout4); //compare
	register_Mem r5(clk,reset,memWrite,decOut[5],32'b 0100001101_110_000_0000000000000000,dataIn,Qout5); //xxx
	register_Mem r6(clk,reset,memWrite,decOut[6],32'b 0001101_110_011_111_01011_01_000_100_001,dataIn,Qout6); //compare
	register_Mem r7(clk,reset,memWrite,decOut[7],32'b 0100001101_100_110_0000000000000000,dataIn,Qout7);  //xor
	
	register_Mem r8(clk,reset,memWrite,decOut[8],32'b 00100_011_01011101_0000000000000000,dataIn,Qout8); //add
	register_Mem r9(clk,reset,memWrite,decOut[9],32'b 0100001101_111_010_0000000000000000,dataIn,Qout9); //xor
	register_Mem r10(clk,reset,memWrite,decOut[10],32'b 0001101_100_111_001_0000000000000000,dataIn,Qout10); //sub
	register_Mem r11(clk,reset,memWrite,decOut[11],32'b 00100_101_11010101_0000000000000000,dataIn,Qout11); //add
	
	register_Mem r12(clk,reset,memWrite,decOut[12],32'b 0001101_110_111_001_0000000000000000,dataIn,Qout12);	//sub
	register_Mem r13(clk,reset,memWrite,decOut[13],32'b 00100_110_01011111_0000000000000000,dataIn,Qout13); //add
	register_Mem r14(clk,reset,memWrite,decOut[14],32'b 0100001101_011_110_0000000000000000,dataIn,Qout14); //xor
	register_Mem r15(clk,reset,memWrite,decOut[15],32'b 00100_111_11111111_0000000000000000,dataIn,Qout15); //add 
	
	mux16to1 mMem (Qout0,Qout1,Qout2,Qout3,Qout4,Qout5,Qout6,Qout7,Qout8,Qout9,Qout10,Qout11,Qout12,Qout13,Qout14,Qout15,pc[5:2],IR);
	assign IR1 = IR[31:16];
	assign IR2 = IR[15:0];
endmodule

//DMem

module DMem(input clk, input reset,input memWrite,input memRead, input [31:0] pc, input [31:0] dataIn,output [31:0] IR);
	wire [31:0] Qout0, Qout1, Qout2, Qout3, Qout4, Qout5, Qout6, Qout7,
					Qout8, Qout9, Qout10, Qout11, Qout12, Qout13, Qout14, Qout15;
	wire [15:0] decOut;
	decoder4to16 dec0( pc[5:2], decOut);
	
	register_Mem r0(clk,reset,memWrite,decOut[0],32'b 01111111111111111111111111111111,dataIn,Qout0);  
	register_Mem r1(clk,reset,memWrite,decOut[1],32'b 10000000000000000000000000000100,dataIn,Qout1); 
	register_Mem r2(clk,reset,memWrite,decOut[2],32'b 00000000000000000000000000000100,dataIn,Qout2);  
	register_Mem r3(clk,reset,memWrite,decOut[3],32'b 00000000000000000000000000001111,dataIn,Qout3);
	
	register_Mem r4(clk,reset,memWrite,decOut[4],32'b 00000000000000000000000000000101,dataIn,Qout4); 
	register_Mem r5(clk,reset,memWrite,decOut[5],32'b 00000000000000000000000000000111,dataIn,Qout5); 
	register_Mem r6(clk,reset,memWrite,decOut[6],32'b 00000000000000000000000000000011,dataIn,Qout6); 
	register_Mem r7(clk,reset,memWrite,decOut[7],32'b 00000000000000000000000000000001,dataIn,Qout7); 
	
	register_Mem r8(clk,reset,memWrite,decOut[8],32'b 00000000000000000000000000001010,dataIn,Qout8); 
	register_Mem r9(clk,reset,memWrite,decOut[9],32'b 00000000000000000000000000001001,dataIn,Qout9); 
	register_Mem r10(clk,reset,memWrite,decOut[10],32'b 11111111111111111111111111111100,dataIn,Qout10);
	register_Mem r11(clk,reset,memWrite,decOut[11],32'b 11111111111111111111111111111101,dataIn,Qout11);
	
	register_Mem r12(clk,reset,memWrite,decOut[12],32'b 00000000000000000000000000001011,dataIn,Qout12);
	register_Mem r13(clk,reset,memWrite,decOut[13],32'b 00000000000000000000000100000000,dataIn,Qout13); 
	register_Mem r14(clk,reset,memWrite,decOut[14],32'b 00000000000000000000000000000010,dataIn,Qout14); 
	register_Mem r15(clk,reset,memWrite,decOut[15],32'b 00000000000000000000000000100000,dataIn,Qout15); 
	
	mux16to1 mMem (Qout0,Qout1,Qout2,Qout3,Qout4,Qout5,Qout6,Qout7,Qout8,Qout9,Qout10,Qout11,Qout12,Qout13,Qout14,Qout15,pc[5:2],IR);
endmodule

//decoder4to16

module decoder4to16( input [3:0] destReg, output reg [15:0] decOut);
	always@(destReg)
	case(destReg)
			4'b0000: decOut=16'b0000000000000001; 
			4'b0001: decOut=16'b0000000000000010;
			4'b0010: decOut=16'b0000000000000100;
			4'b0011: decOut=16'b0000000000001000;
			4'b0100: decOut=16'b0000000000010000;
			4'b0101: decOut=16'b0000000000100000;
			4'b0110: decOut=16'b0000000001000000;
			4'b0111: decOut=16'b0000000010000000;
			4'b1000: decOut=16'b0000000100000000; 
			4'b1001: decOut=16'b0000001000000000;
			4'b1010: decOut=16'b0000010000000000;
			4'b1011: decOut=16'b0000100000000000;
			4'b1100: decOut=16'b0001000000000000;
			4'b1101: decOut=16'b0010000000000000;
			4'b1110: decOut=16'b0100000000000000;
			4'b1111: decOut=16'b1000000000000000;
	endcase
endmodule

//mux16to1

module mux16to1( input [31:0] outR0,outR1,outR2,outR3,outR4,outR5,outR6,outR7,outR8,outR9,outR10,outR11,outR12,outR13,outR14,outR15, input [3:0] Sel, output reg [31:0] outBus );
	always@(outR0 or outR1 or outR2 or outR3 or outR4 or outR5 or outR6 or outR7 or outR8 or outR9 or outR10 or outR11 or outR12 or outR13 or outR14 or outR15 or Sel)
	case (Sel)
				4'b0000: outBus=outR0;
				4'b0001: outBus=outR1;
				4'b0010: outBus=outR2;
				4'b0011: outBus=outR3;
				4'b0100: outBus=outR4;
				4'b0101: outBus=outR5;
				4'b0110: outBus=outR6;
				4'b0111: outBus=outR7;
				4'b1000: outBus=outR8;
				4'b1001: outBus=outR9;
				4'b1010: outBus=outR10;
				4'b1011: outBus=outR11;
				4'b1100: outBus=outR12;
				4'b1101: outBus=outR13;
				4'b1110: outBus=outR14;
				4'b1111: outBus=outR15;
	endcase
endmodule

//mux8to1_8bits

module mux8to1_8_bit( input [7:0] outR0,input [7:0] outR1,input [7:0] outR2,input [7:0] outR3,input [7:0] outR4,input [7:0] outR5,input [7:0] outR6,input [7:0] outR7,input [2:0] Sel, output reg [7:0] outBus );
	always@(outR0 or outR1 or outR2 or outR3 or outR4 or outR5 or outR6 or outR7 or Sel)
	case (Sel)
				3'b000: outBus=outR0;
				3'b001: outBus=outR1;
				3'b010: outBus=outR2;
				3'b011: outBus=outR3;
				3'b100: outBus=outR4;
				3'b101: outBus=outR5;
				3'b110: outBus=outR6;
				3'b111: outBus=outR7;
				endcase
endmodule

//mux2to1_3bits

module mux2to1_3_bits( input [2:0] outR0,input [2:0] outR1, input Sel, output reg [2:0] outBus );
	always@(outR0 or outR1 or Sel)
	case (Sel)
				1'b0: outBus=outR0;
				1'b1: outBus=outR1;
				endcase
endmodule

//mux2to1_32bits

module mux2to1_32bits(input [31:0] in1, input [31:0] in2,input sel, output reg [31:0] muxout);
	 always@(in1 or in2 or sel)
	 begin
		case(sel)
			1'b0 : muxout = in1;
			1'b1 : muxout = in2;
			endcase
	 end
endmodule


//mux4to1_32bits

module mux4to1_32bits(input [31:0] in1, input [31:0] in2, input [31:0] in3,input [31:0] in4, input [1:0] sel, output reg [31:0] muxout);
	 always@(in1 or in2 or in3 or in4 or sel)
	 begin
		case(sel)
			2'b00 : muxout = in1;
			2'b01 : muxout = in2;
			2'b10 : muxout = in3;
			2'b11 : muxout = in4;
		endcase
	 end
endmodule


module mux8to1_32bits(input [31:0] in1, input [31:0] in2, input [31:0] in3,input [31:0] in4, input [31:0] in5, input [31:0] in6, input [31:0] in7,input [31:0] in8, input [2:0] sel, output reg [31:0] muxout);
	 always@(in1 or in2 or in3 or in4 or in5 or in6 or in7 or in8 or sel)
	 begin
		case(sel)
			3'b000 : muxout = in1;
			3'b001 : muxout = in2;
			3'b010 : muxout = in3;
			3'b011 : muxout = in4;
			3'b100 : muxout = in5;
			3'b101 : muxout = in6;
			3'b110 : muxout = in7;
			3'b111 : muxout = in8;
		endcase
	 end
endmodule


module register32bit( input clk, input reset, input regWrite1, input regWrite2, input decOut1b1, input decOut1b2, input [31:0] writeData1, input [31:0] writeData2, output  [31:0] outR );
	D_ff d0(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[0], writeData2[0], outR[0]);
	D_ff d1(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[1], writeData2[1], outR[1]);
	D_ff d2(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[2], writeData2[2], outR[2]);
	D_ff d3(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[3], writeData2[3], outR[3]);
	D_ff d4(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[4], writeData2[4], outR[4]);
	D_ff d5(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[5], writeData2[5], outR[5]);
	D_ff d6(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[6], writeData2[6], outR[6]);
	D_ff d7(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[7], writeData2[7], outR[7]);
	D_ff d8(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[8], writeData2[8], outR[8]);
	D_ff d9(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[9], writeData2[9], outR[9]);
	D_ff d10(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[10], writeData2[10], outR[10]);
	D_ff d11(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[11], writeData2[11], outR[11]);
	D_ff d12(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[12], writeData2[12], outR[12]);
	D_ff d13(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[13], writeData2[13], outR[13]);
	D_ff d14(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[14], writeData2[14], outR[14]);
	D_ff d15(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[15], writeData2[15], outR[15]);
	D_ff d16(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[16], writeData2[16], outR[16]);
	D_ff d17(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[17], writeData2[17], outR[17]);
	D_ff d18(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[18], writeData2[18], outR[18]);
	D_ff d19(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[19], writeData2[19], outR[19]);
	D_ff d20(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[20], writeData2[20], outR[20]);
	D_ff d21(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[21], writeData2[21], outR[21]);
	D_ff d22(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[22], writeData2[22], outR[22]);
	D_ff d23(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[23], writeData2[23], outR[23]);
	D_ff d24(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[24], writeData2[24], outR[24]);
	D_ff d25(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[25], writeData2[25], outR[25]);
	D_ff d26(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[26], writeData2[26], outR[26]);
	D_ff d27(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[27], writeData2[27], outR[27]);
	D_ff d28(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[28], writeData2[28], outR[28]);
	D_ff d29(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[29], writeData2[29], outR[29]);
	D_ff d30(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[30], writeData2[30], outR[30]);
	D_ff d31(clk, reset, regWrite1, regWrite2, decOut1b1, decOut1b2, writeData1[31], writeData2[31], outR[31]);
endmodule

module register_Set( input clk, input reset, input regWrite1 ,input regWrite2, input [7:0] decOut1,  input [7:0] decOut2, input [31:0] writeData1, input [31:0] writeData2,  output [31:0] outR0, output [31:0] outR1, output [31:0] outR2,output [31:0] outR3,output [31:0] outR4,output [31:0] outR5,output [31:0] outR6,output [31:0] outR7);

register32bit r0(clk, reset, regWrite1, regWrite2, decOut1[0], decOut2[0], writeData1, writeData2, outR0);
register32bit r1(clk, reset, regWrite1, regWrite2, decOut1[1], decOut2[1], writeData1, writeData2, outR1);
register32bit r2(clk, reset, regWrite1, regWrite2, decOut1[2], decOut2[2], writeData1, writeData2, outR2);
register32bit r3(clk, reset, regWrite1, regWrite2, decOut1[3], decOut2[3], writeData1, writeData2, outR3);
register32bit r4(clk, reset, regWrite1, regWrite2, decOut1[4], decOut2[4], writeData1, writeData2, outR4);
register32bit r5(clk, reset, regWrite1, regWrite2, decOut1[5], decOut2[5], writeData1, writeData2, outR5);
register32bit r6(clk, reset, regWrite1, regWrite2, decOut1[6], decOut2[6], writeData1, writeData2, outR6);
register32bit r7(clk, reset, regWrite1, regWrite2, decOut1[7], decOut2[7], writeData1, writeData2, outR7);

endmodule



module registerFile(input clk, input reset, input regWrite1, input regWrite2, input [2:0] srcRs1, input [2:0] srcRt1, input [2:0] srcRs2, input [2:0] srcRt2, input [2:0] srcRd2, input [2:0] destRd1, input [2:0] destRd2, input [31:0] writeData1, input [31:0] writeData2, output [31:0] outRs1, output [31:0] outRt1, output [31:0] outRs2, output [31:0] outRt2, output [31:0] outRd2);
	
	wire [7:0] decOut1, decOut2;
	wire [31:0] outR0,outR1,outR2,outR3,outR4,outR5,outR6,outR7;
	
	decoder3to8 dec1(destRd1, decOut1);
	decoder3to8 dec2(destRd2, decOut2);
	register_Set rSet(clk, reset, regWrite1, regWrite2, decOut1, decOut2, writeData1, writeData2, outR0,outR1,outR2,outR3,outR4,outR5,outR6,outR7);
	mux8to1_32bits m1(outR0,outR1,outR2,outR3,outR4,outR5,outR6,outR7, srcRs1, outRs1);
	mux8to1_32bits m2(outR0,outR1,outR2,outR3,outR4,outR5,outR6,outR7, srcRt1, outRt1);
	mux8to1_32bits m3(outR0,outR1,outR2,outR3,outR4,outR5,outR6,outR7, srcRs2, outRs2);
	mux8to1_32bits m4(outR0,outR1,outR2,outR3,outR4,outR5,outR6,outR7, srcRt2, outRt2);
	mux8to1_32bits m5(outR0,outR1,outR2,outR3,outR4,outR5,outR6,outR7, srcRd2, outRd2);
	
endmodule


module shiftLeft_1(input [31:0] shiftLeft_1_input,output reg [31:0] shiftLeft_1_output);
	always@(shiftLeft_1_input)
	begin
	  shiftLeft_1_output = shiftLeft_1_input << 1'b1;
	end
endmodule

module shiftLeft_2(input [27:0] shiftLeft_1_input,output reg [27:0] shiftLeft_1_output);
	always@(shiftLeft_1_input)
	begin
	  shiftLeft_1_output = shiftLeft_1_input << 1'b1;
	end
endmodule


module signExt8to32( input [7:0] offset, output reg [31:0] signExtOffset);
	always@(offset)
	begin
			signExtOffset={{24{offset[7]}},offset[7:0]};
	end
endmodule


module signExt11to28( input [10:0] offset, output reg [27:0] signExtOffset);
	always@(offset)
	begin
			signExtOffset={{17{offset[10]}},offset[10:0]};
	end
endmodule


module forwarding_unit(input p3_regWrite1, input p3_regWrite2, input p4_regWrite1, input p4_regWrite2, input [2:0] p2_RsI1, input [2:0] p2_RtI1, input [2:0] p2_RsI2, input [2:0] p2_RtI2, input [2:0] p2_RdI2, input [2:0] p3_RdI1, input [2:0] p3_RdI2, input [2:0] p4_RdI1, input [2:0] p4_RdI2, output reg [1:0] sel_alu_m1, output reg [1:0] sel_alu_m2, output reg [1:0] sel_adder_m1, output reg [1:0] sel_adder_m2, output reg [1:0] sel_Rd_for_DM);
wire C11, C12, C21, C22, D11, D12, D21, D22;

// C12 means instruction 1 is dependent on instruction 2, and C -> Rs
// D12 means instruction 1 is dependent on instruction 2, and D -> Rt

assign C11 = p3_regWrite1 && (p3_RdI1 == p2_RsI1);
assign D11 = p3_regWrite1 && (p3_RdI1 == p2_RtI1);

assign C21 = p3_regWrite1 && (p3_RdI1 == p2_RsI2);
assign D21 = p3_regWrite1 && (p3_RdI1 == p2_RtI2);

assign C12 = p3_regWrite2 && (p3_RdI2 == p2_RsI1);
assign D12 = p3_regWrite2 && (p3_RdI2 == p2_RtI1);

assign C22 = p3_regWrite2 && (p3_RdI2 == p2_RsI2);
assign D22 = p3_regWrite2 && (p3_RdI2 == p2_RtI2);

assign E21 = p3_regWrite1 && (p3_RdI1 == p2_RdI2);
assign E22 = p3_regWrite2 && (p3_RdI2 == p2_RdI2);

always @ (p3_regWrite1, p3_regWrite2, p4_regWrite1, p4_regWrite2, p2_RsI1, p2_RtI1, p2_RsI2, p2_RtI2, p3_RdI1, p3_RdI2, p4_RdI1, p4_RdI2, p2_RdI2)

	begin
	sel_alu_m1 = 2'b00;
	sel_alu_m2 = 2'b00;
	sel_adder_m1 = 2'b00;
	sel_adder_m2 = 2'b00;
	sel_Rd_for_DM = 2'b00;
	
	// EX Hazard
	
	//1-1										// 1-2 and 2-2 are cases of Load Use Hazard
	if(C11)
	begin
		sel_alu_m1 = 2'b01;
	end
	
	if(D11)
	begin
		sel_alu_m2 = 2'b01;
	end
	
	//2-1
	if(C21)
	begin
		sel_adder_m1 = 2'b01;
	end
	
	if(D21)
	begin
		sel_adder_m2 = 2'b01;
	end
	
	
	if(E21)
	begin
		sel_Rd_for_DM = 2'b01;
	end
	
	//Mem Hazard
	
	//1-1
	if(p4_regWrite1 &&  (p4_RdI1 == p2_RsI1) && !(C11 || C12))
	begin
		sel_alu_m1 = 2'b10;
	end
	
	if(p4_regWrite1 &&  (p4_RdI1 == p2_RtI1) && !(D11 || D12))
	begin
		sel_alu_m2 = 2'b10;
	end
	
	//2-1
	if(p4_regWrite1 && (p4_RdI1 == p2_RsI2) && !(C21 || C22))
	begin
		sel_adder_m1 = 2'b10;
	end
	
	if(p4_regWrite1 && (p4_RdI1 == p2_RtI2) && !(D21 || D22))
	begin
		sel_adder_m2 = 2'b10;
	end
	
	//1-2
	if(p4_regWrite2 && (p4_RdI2 == p2_RsI1) && !(C12 || C11))
	begin
		sel_alu_m1 = 2'b11;
	end
	
	if(p4_regWrite2 && (p4_RdI2 == p2_RtI1) && !(D12 || D11))
	begin
		sel_alu_m2 = 2'b11;
	end
	
	//2-2
	if(p4_regWrite2 && (p4_RdI2 == p2_RsI2) && !(C22 || C21))
	begin
		sel_adder_m1 = 2'b11;
	end
	
	if(p4_regWrite2 && (p4_RdI2 == p2_RtI2) && !(D22 || D21))
	begin
		sel_adder_m2 = 2'b11;
	end
	
	// --
	if(p4_regWrite2 && (p4_RdI2 == p2_RdI2) && !(E21))
	begin
		sel_Rd_for_DM = 2'b11;
	end
	
	if(p4_regWrite2 && (p4_RdI1 == p2_RdI2) && !(E21))
	begin
		sel_Rd_for_DM = 2'b10;
	end
end
endmodule


module load_flag(input memRead, input [31:0] memOut, input old_Z, input old_N, output reg new_Z, output reg new_N);
	always@(memRead, memOut, old_Z, old_N)
	begin
	new_N = old_N;
	new_Z = old_Z;
	if(memRead == 1'b1)
	 begin
		if(memOut == 32'b0)
			new_Z = 1'b1;
		else
			new_Z = old_Z;
		
		if(memOut[31] == 1'b1)
		  begin
			new_N = 1'b1;
			end
		else
		  begin
			new_N = old_N;
			end
	end
end
endmodule

module hazard_detection_unit(input [4:0] opI1, input [4:0] opI2, input is_exception, input [2:0] RsI1, input [2:0] RtI1, input [2:0] RsI2, input [2:0] RtI2, input [2:0] RdI2, input [2:0] p1_RdI2, input p1_memReadI2, input f1_or_f2, output reg PCWrite, output reg IFIDWrite, output reg ctr_sig_mux, output reg Flush);
//store_or_load = {IR2[15],IR2[11]}
always @(opI1, opI2, is_exception, RsI1,RtI1, RsI2,RtI2,RdI2, p1_RdI2,p1_memReadI2, f1_or_f2)
	begin
	 PCWrite = 1'b1;
	 IFIDWrite = 1'b1;
	 ctr_sig_mux = 1'b1;
	 Flush = 1'b0;
	 if(opI2==5'b11010 && (f1_or_f2))
	 begin
		  IFIDWrite=1'b0;
		  ctr_sig_mux = 0;
		  PCWrite=0;
	 end
		
	 if(is_exception)
	 begin
		  Flush = 1'b1;
	 end
	 
	 if(opI1 == 5'b00100) //if add
	 begin
			if((p1_memReadI2 == 1'b1) && (p1_RdI2 == RsI1))
			begin
				PCWrite = 0;
				IFIDWrite = 0;
				ctr_sig_mux = 0; // Make ctr signals 0
			end
		end
	  else
	  begin
		  if(opI1 == 5'b00011 || opI1 == 5'b01000)
		  begin
			 if((p1_memReadI2 == 1'b1) && ((p1_RdI2 == RsI1) || (p1_RdI2 == RtI1)))
			 begin
				PCWrite = 0;
				IFIDWrite = 0;
				ctr_sig_mux = 0; // Make ctr signals 0
			 end
		  end
	  end
	  
	if(opI2 == 5'b01010) //load
	begin
		if((p1_memReadI2 == 1'b1) && ((p1_RdI2 == RsI2) || (p1_RdI2 == RtI2)))
			begin
				PCWrite = 0;
				IFIDWrite = 0;
				ctr_sig_mux = 0; // Make ctr signals 0
			end
	end
	
	if(opI2 == 5'b01011) // store
	begin
		if((p1_memReadI2 == 1'b1) && ((p1_RdI2 == RsI2) || (p1_RdI2 == RtI2) || (p1_RdI2 == RdI2)))
		begin
			PCWrite = 0;
			IFIDWrite = 0;
			ctr_sig_mux = 0;
		end
	end
	
end
endmodule			

module Question15(input clk, input reset, output [31:0] p3_aluOut, output [31:0] p3_memOut);

// PC
wire [31:0] PCOut, PCIn; 
wire PCWrite;
//PCadder
wire [31:0] PCadderOut;
//PCmux
wire [1:0] PCSrc;
//IM
wire [15:0] IR1, IR2;
//IF_ID
wire IFIDregWrite;
wire [15:0] p0_IR1, p0_IR2;
wire [31:0] p0_PC;
// muxRsI1
wire [2:0] RsI1;
// muxRtI1
wire [2:0] RtI1;
// sext_jmp
wire [27:0] signExt_jmp;
// sext_branch
wire [31:0] signExt_branch;
// mux_jmpbranch
wire ctr_branchI2;
wire [31:0] jmp_br_address;
//shl
wire [31:0] shl_output;
// jmp_br_adder
wire [31:0] jmp_br_pc;
// muxRdI1
wire [2:0] RdI1;
// ctrl_ckt
wire ctr_jmpI2, ctr_memReadI2, ctr_memWriteI2,  ctr_regWriteI1, ctr_regWriteI2, ctr_undef, ctr_f1, ctr_f2;
wire [1:0] ctr_aluOpI1;
//flag registers
wire Zoutput, Noutput, Coutput, Voutput;
// rf
wire p3_regWriteI1, p3_regWriteI2;
wire [2:0] p3_RdI1, p3_RdI2;
wire [31:0] regRsI1, regRtI1, regRsI2, regRtI2, regRdI2;
// id_ex
wire IDEXWrite, p1_f1, p1_f2;
wire [31:0] p1_PC, p1_regRsI1, p1_regRtI1, p1_regRsI2, p1_regRtI2, p1_regRdI2;
wire [2:0] p1_RsI1, p1_RtI1, p1_RdI1, p1_RsI2, p1_RtI2, p1_RdI2;
wire [7:0] p1_imm_I1;
wire [1:0] p1_aluOpI1;
wire p1_regDstI1, p1_memRead, p1_memWrite, p1_regWriteI1, p1_regWriteI2, p1_undef, p1_is_add;
// alu_m1
wire [31:0] p2_aluOut, alu_in1;
wire [1:0] sel_alu_m1;
// alu_m2
wire [31:0] alu_in2;
wire [1:0] sel_alu_m2;
// adder_m1
wire [31:0] adder_in1;
wire [1:0] sel_adder_m1;
// adder_m2
wire [31:0] adder_in2;
wire [1:0] sel_adder_m2;
// sext_alu
wire [31:0] signExt_alu;
//mux_imm
wire [31:0] mux_imm_Out;
//alu
wire [31:0] aluOut;
wire alu_Z, alu_N, alu_C, alu_V;
//adder
wire [31:0] adderOut;
//ex_mem
wire EXMemWrite, p2_memRead, p2_memWrite, p2_regWriteI1, p2_regWriteI2, p2_Z, p2_N, p2_C, p2_V, p2_f1, p2_f2;
wire [2:0] p2_RdI1, p2_RdI2;
wire [31:0] p2_regRdI2, p2_adderOut;
//DM
wire [31:0] DM_Out;
//lfchecker
wire lf_Z, lf_N;
// mem_wb
wire MemWbWrite, p3_Z, p3_N, p3_C, p3_V, p3_f1, p3_f2;
//epc & cause
wire [31:0] epc_out;
wire cause_out;
//mux_for_rd_store
wire [31:0] rd_for_store;
wire [1:0] sel_Rd_for_DM;
wire [27:0] sh2_output;
//Pipeline Writes (Change when Hazard is made)
assign IDEXWrite = 1'b1;
assign EXMemWrite = 1'b1;
assign MemWbWrite = 1'b1;

//assign PCSrc ={ p1_undef || alu_V, ctr_jmpI2 || (ctr_branchI2 && !(p2_N))};
//assign PCWrite = 1'b1; //Assuming hazard is not completed

//HDU
wire ctr_sig_mux, Flush;

	register32bit_p PC(clk, reset, PCWrite, 1'b1, PCIn, PCOut );
	Mem IM(clk, reset, 1'b0, 1'b1, PCOut, 32'b0, IR1, IR2);
	adder PCadder(32'b100, PCOut, PCadderOut);
	mux4to1_32bits PCmux(PCadderOut, jmp_br_pc, 32'b10000000000000000000000110000000, {p0_PC[31:28],sh2_output}, PCSrc, PCIn);
	IF_ID if_id(clk, Flush||reset, IFIDregWrite, 1'b1, IR1, IR2, PCadderOut, p0_IR1, p0_IR2, p0_PC);
	mux2to1_3_bits muxRsI1( p0_IR1[5:3], p0_IR1[10:8], p0_IR1[13], RsI1);
	mux2to1_3_bits muxRtI1(p0_IR1[8:6], p0_IR1[2:0], p0_IR1[14], RtI1);
	signExt11to28 sext_jmp( p0_IR2[10:0], signExt_jmp);
	signExt8to32 sext_branch( p0_IR2[7:0] , signExt_branch);
	//mux2to1_32bits mux_jmpbranch({p0_PC[31:28],signExt_jmp}, signExt_branch, ctr_branchI2, jmp_br_address); // Doubt <<1 or <<2
	shiftLeft_1 shl(signExt_branch, shl_output);
	shiftLeft_2 sh2(signExt_jmp, sh2_output);
	adder jmp_br_adder(shl_output, p0_PC, jmp_br_pc);
	mux2to1_3_bits muxRdI1( p0_IR1[2:0], p0_IR1[10:8], p0_IR1[13], RdI1);
	ctrlCkt	ctrl_ckt(p0_IR1[15:11], p0_IR2[15:11], p0_IR1[9], p1_undef || alu_V, ctr_memReadI2, ctr_aluOpI1, ctr_memWriteI2,  ctr_regWriteI1, ctr_regWriteI2, ctr_undef, ctr_f1, ctr_f2, PCSrc);
	register1bit_p Zflag(clk, reset, p3_f1 || p3_f2, 1'b1, lf_Z, Zoutput);
	register1bit_p Nflag(clk, reset, p3_f1 || p3_f2, 1'b1, lf_N, Noutput);
	register1bit_p Cflag(clk, reset, p3_f1, 1'b1, p3_C, Coutput);
	register1bit_p Vflag(clk, reset, p3_f1, 1'b1, p3_V, Voutput);
	registerFile rf(clk, reset, p3_regWriteI1, p3_regWriteI2, RsI1, RtI1, p0_IR2[8:6], p0_IR2[5:3], p0_IR2[2:0], p3_RdI1, p3_RdI2, p3_aluOut, p3_memOut, regRsI1, regRtI1, regRsI2, regRtI2, regRdI2);
	ID_EX id_ex(clk, Flush||reset, IDEXWrite, 1'b1, p0_PC, regRsI1,regRtI1,regRsI2,regRtI2,regRdI2,RsI1,RtI1, RdI1, p0_IR2[8:6],p0_IR2[5:3], p0_IR2[2:0], p0_IR1[7:0], ctr_aluOpI1, ctr_undef && ctr_sig_mux, ctr_memReadI2 && ctr_sig_mux, ctr_memWriteI2 && ctr_sig_mux, ctr_regWriteI1 && ctr_sig_mux, ctr_regWriteI2 && ctr_sig_mux, p0_IR1[13] && ctr_sig_mux, ctr_f1 && ctr_sig_mux, ctr_f2 && ctr_sig_mux,
                p1_PC, p1_regRsI1, p1_regRtI1, p1_regRsI2, p1_regRtI2, p1_regRdI2, p1_RsI1, p1_RtI1, p1_RdI1, p1_RsI2, p1_RtI2, p1_RdI2, p1_imm_I1, p1_aluOpI1, p1_memRead, p1_memWrite, p1_regWriteI1, p1_regWriteI2, p1_undef, p1_is_add, p1_f1, p1_f2);
	register32bit_p epc(clk, reset, p1_undef || alu_V, 1'b1, p1_PC, epc_out);
	register1bit_p cause(clk, reset, p1_undef || alu_V, 1'b1, !(p1_undef), cause_out);
	 mux4to1_32bits alu_m1(p1_regRsI1, p2_aluOut, p3_aluOut, p3_memOut, sel_alu_m1, alu_in1);
	 mux4to1_32bits alu_m2(mux_imm_Out, p2_aluOut, p3_aluOut, p3_memOut, sel_alu_m2, alu_in2);
	 mux4to1_32bits adder_m1(p1_regRsI2, p2_aluOut, p3_aluOut, p3_memOut, sel_adder_m1, adder_in1);
	 mux4to1_32bits adder_m2(p1_regRtI2, p2_aluOut, p3_aluOut, p3_memOut, sel_adder_m2, adder_in2);
	 mux4to1_32bits mux_for_rd_DM(p1_regRdI2, p2_aluOut, p3_aluOut, p3_memOut, sel_Rd_for_DM, rd_for_store);
	 forwarding_unit funit(p2_regWriteI1, p2_regWriteI2, p3_regWriteI1, p3_regWriteI2, p1_RsI1, p1_RtI1, p1_RsI2, p1_RtI2, p1_RdI2, p2_RdI1, p2_RdI2, p3_RdI1, p3_RdI2, sel_alu_m1, sel_alu_m2, sel_adder_m1, sel_adder_m2, sel_Rd_for_DM);
	 signExt8to32 sext_alu( p1_imm_I1 , signExt_alu);
	 mux2to1_32bits mux_imm(p1_regRtI1, signExt_alu, p1_is_add, mux_imm_Out);
	 alu al(alu_in1, alu_in2, p1_aluOpI1, aluOut, alu_Z, alu_N, alu_C, alu_V);
	 adder ex_adder(adder_in1, adder_in2, adderOut);
	 EX_MEM ex_mem(clk, Flush||reset, EXMemWrite, 1'b1,aluOut,adderOut,p1_memRead, p1_memWrite,p1_regWriteI1,p1_regWriteI2, p1_RdI1, p1_RdI2, rd_for_store,alu_Z, alu_N, alu_C,alu_V, p1_f1, p1_f2,
				   p2_aluOut, p2_adderOut, p2_memRead, p2_memWrite, p2_regWriteI1, p2_regWriteI2, p2_RdI1, p2_regRdI2, p2_RdI2, p2_Z, p2_N, p2_C, p2_V, p2_f1, p2_f2 );
	 DMem DM(clk,reset, p2_memWrite, p2_memRead, p2_adderOut, p2_regRdI2, DM_Out);
	 load_flag lfchecker(p3_regWriteI2, p3_memOut, p3_Z, p3_N, lf_Z, lf_N);
	 MEM_WB mem_wb(clk,reset,MemWbWrite, 1'b1, p2_aluOut, DM_Out, p2_regWriteI1, p2_regWriteI2, p2_RdI1, p2_RdI2, p2_Z, p2_N, p2_C, p2_V, p2_f1, p2_f2,
				p3_aluOut, p3_RdI1, p3_RdI2, p3_memOut, p3_regWriteI1, p3_regWriteI2, p3_Z, p3_N, p3_C, p3_V, p3_f1, p3_f2 );
	hazard_detection_unit HDU( p0_IR1 [15:11], p0_IR2 [15:11], p1_undef || alu_V, RsI1, RtI1, p0_IR2[8:6], p0_IR2[5:3], p0_IR2[2:0], p1_RdI2, p1_memRead, p1_f1 || p1_f2,
	PCWrite, IFIDregWrite, ctr_sig_mux, Flush);

				
endmodule


module pipelineTestBench;
	reg clk;
	reg reset;
	wire [31:0] p3_aluOut, p3_memOut;
  Question15 uut(.clk(clk), .reset(reset), .p3_aluOut(p3_aluOut), .p3_memOut(p3_memOut));
	always
	#5 clk=~clk;
	
	initial
	begin
		clk=0; reset=1;
		#10  reset=0;	
		 
	end
endmodule




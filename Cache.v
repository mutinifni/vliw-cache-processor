module And_4_Array(
                      input            a,
                      input[3:0]      b,
                      output[3:0]     out
                    );

and a0(out[0], b[0], a);
and a1(out[1], b[1], a);
and a2(out[2], b[2], a);
and a3(out[3], b[3], a);
endmodule                    

module Block_Cache_4Word(  
                            input                 clk,
                            input                 reset,
                            input[3:0]	          writeSignal,
                            input[127:0]          init,  
                            input[127:0]          writeData,    //Top level will multiplex mem byte with CPU byte
                            output[127:0]           val                          
                          );
//Check if write Signal should be the same for all the registers.
  Word_Register_Cache b0(clk,reset,writeSignal[0], init[31:0], writeData[31:0],val[31:0]);         
  Word_Register_Cache b1(clk,reset,writeSignal[1], init[63:32], writeData[63:32],val[63:32]);         
  Word_Register_Cache b2(clk,reset,writeSignal[2], init[95:64], writeData[95:64],val[95:64]);         
  Word_Register_Cache b3(clk,reset,writeSignal[3], init[127:96], writeData[127:96],val[127:96]);                                                    
  
endmodule     
                     
module counter(
                input clk, 
                input reset, 
                input dec, 
                input load, 
                output [2:0] val
              );

  //A three bit counter used in the LRU-counter replacement strategy
  //Since associativity of the cache is 8, the counter size is log 8 = 3
  
  //Operations supported by the counter, in decreasing order of precedence are -
  //  Reset - Used at the beginning to reset the counter to 3'b000
  //  Load  - Used when there is a cache hit or miss to set the counter to 3'b111
  //  Dec   - Used to decrement the value of the counter
  
  //Counter is negative edge triggered and implemented using D flip flops

  reg[2:0] temp;
  D_FF_Counter d0(temp[0], val[0]);
  D_FF_Counter d1(temp[1], val[1]);
  D_FF_Counter d2(temp[2], val[2]);
  
  always @(negedge clk)
  begin
    temp = val;
    if(reset == 1)
        temp = 3'b000;     //Set count to 2'b00
    else if(load == 1)
        temp = 3'b111;     //Set count to 2'b11
    else if(dec == 1 && temp != 0)  //Is the temp != 0 check really needed?
        temp = temp - 1;  //Decrement the count
  end

endmodule

module D_FF_Cache(  
                    input          clk,
                    input          reset,
                    input          writeSignal, 
                    input          writeData,
                    output reg     q
                  );

always @(posedge clk)
begin
  if(reset == 1)
    q = 0;
  else if(writeSignal == 1)
    q = writeData;
end
                  
endmodule                  

module D_FF_Cache_withInit(  
                    input          clk,
                    input          reset,
                    input          writeSignal,
					          input		        init, 					
                    input          writeData,
                    output reg     q
                  );

always @(posedge clk)
begin
  if(reset == 1)
    q = init;
  else if(writeSignal == 1)
    q = writeData;
end
                  
endmodule    
		   
module  D_FF_Counter( 
                      input       d,
                      output reg  val
                    );
  
  //Implementation of a D flip flop for the counter used in LRU-counter replacement strategy
  //Since counting and resetting is done at the counter level, this module simply reflects
  //the value of d supplied to it  
  //This implementation has been adopted from lab2
         
  always @(d)
  begin
    val = d;
  end         
                    
endmodule 

module Data_Array(
                        input                   clk,
                        input                   reset,
                        input 					CPUWrite,
                        input 					replace,
                        input[3:0]				offset,
                        input[1:0]				index,
                        input[127:0]            init0,
                        input[127:0]            init1,
                        input[127:0]            init2,
                        input[127:0]            init3,
                        input[127:0]            writeData,		
                        input[31:0]				CPUOut,
                        output[127:0]           val0, 
                        output[127:0]           val1,
                        output[127:0]           val2,
                        output[127:0]           val3
                      );
//Here writesignal is (miss && (CPURead || CPUWrite)) || (Hit && CPUWrite && offsetDecoderOutput[i])
wire[127:0] dataWrite;
wire[3:0]	write, muxOut, setDecoderOut, offsetDecoderOut, blockwrite0, blockwrite1, blockwrite2, blockwrite3;
wire muxL1Out_0,muxL1Out_1,muxL1Out_2,muxL1Out_3;
decoder2to4 d(offset[3:2],offsetDecoderOut);
mux2to1_4bits m(offsetDecoderOut, 4'b1111, replace, muxOut);

or(writeSignal, CPUWrite, replace);

mux2to1_1bit m1_0(1'b0, offsetDecoderOut[0], CPUWrite, muxL1Out_0);
mux2to1_1bit m1_1(1'b0, offsetDecoderOut[1], CPUWrite, muxL1Out_1);
mux2to1_1bit m1_2(1'b0, offsetDecoderOut[2], CPUWrite, muxL1Out_2);
mux2to1_1bit m1_3(1'b0, offsetDecoderOut[3], CPUWrite, muxL1Out_3);

mux2to1_32bits m20(writeData[31:0] , CPUOut, muxL1Out_0, dataWrite[31:0]);
mux2to1_32bits m21(writeData[63:32] , CPUOut, muxL1Out_1, dataWrite[63:32]);
mux2to1_32bits m22(writeData[95:64] , CPUOut, muxL1Out_2, dataWrite[95:64]);
mux2to1_32bits m23(writeData[127:96] , CPUOut, muxL1Out_3, dataWrite[127:96]);

decoder2to4 d2(index,setDecoderOut);

And_4_Array a0(writeSignal,muxOut,write);


And_4_Array aa0(setDecoderOut[0], write, blockwrite0);
And_4_Array aa1(setDecoderOut[1], write, blockwrite1);
And_4_Array aa2(setDecoderOut[2], write, blockwrite2);
And_4_Array aa3(setDecoderOut[3], write, blockwrite3);

Block_Cache_4Word b0(
					clk,
					reset,
					blockwrite0, 
					init0,
					dataWrite, 
					val0);
Block_Cache_4Word b1(
					clk,
					reset,
					blockwrite1, 
					init1,
					dataWrite, 
					val1);
Block_Cache_4Word b2(
					clk,
					reset,
					blockwrite2,
					init2,
					dataWrite, 
					val2);
Block_Cache_4Word b3(
					clk,
					reset,
					blockwrite3,
					init3,					
					dataWrite, 
					val3);
					
endmodule

module Tag_Array(	
				input 			clk,
				input 			reset,
				input[3:0]      writeSignal,
				input[25:0]		writeTag,
				input[25:0]  init0,
				input[25:0]  init1,
				input[25:0]  init2,
				input[25:0]  init3,
				
				output[25:0]	tag0,
				output[25:0]	tag1,
				output[25:0]	tag2,
				output[25:0]	tag3
				);

Tag_Register_Cache t0(
                                clk,
                                reset,
                                writeSignal[0],
								init0,
                                writeTag,
                                tag0
                              );
                              

Tag_Register_Cache t1(
                                clk,
                                reset,
                                writeSignal[1],
								init1,
                                writeTag,
                                tag1
                              );
        

Tag_Register_Cache t2(
                                clk,
                                reset,
                                writeSignal[2],
								init2,
                                writeTag,
                                tag2
                              );

Tag_Register_Cache t3(
                                clk,
                                reset,
                                writeSignal[3],
								init3,
                                writeTag,
                                tag3
                              );
         

endmodule

module Valid_Array(
					input 			clk,
					input 			reset,
					input[3:0] 		writeSignal,
					input[3:0]   init,
					input[3:0]		 validInput,
					output[3:0]		validOutput
					);

D_FF_Cache_withInit d0(clk,reset,writeSignal[0], init[0], validInput[0],validOutput[0]);
D_FF_Cache_withInit d1(clk,reset,writeSignal[1], init[1], validInput[1],validOutput[1]);
D_FF_Cache_withInit d2(clk,reset,writeSignal[2], init[2], validInput[2],validOutput[2]);
D_FF_Cache_withInit d3(clk,reset,writeSignal[3], init[3], validInput[3],validOutput[3]);

endmodule


module LRU_Counter_Replacement(
                                input             clk,
                                input             reset,
                                input[2:0]        lineIndex,
                                input             setInUse,
                                input             hit,
                                input             CPU,
                                output[2:0]       outIndex
                              );
  //CPU = CPURead | CPUWrite         
  //To avoid incrementing counters during non memory operations
  
  //LRU Counter replacement strategy implementation using 8 3-bit counters
  
  //Used to select which line in a specific block should be replaced
  
  //On hit, the corresponding line's counter is set to 3'b111, and all other counters with value
  //strictly greater than the old counter value are decremented by 1.
  
  wire[2:0]   lineOrOutIndex;
  wire[7:0]   decoderOut;
  wire        dec0, dec1, dec2, dec3, dec4, dec5, dec6, dec7;
  wire        dec0_set, dec1_set, dec2_set, dec3_set, dec4_set, dec5_set, dec6_set, dec7_set;
  wire        dec0_set_read, dec1_set_read, dec2_set_read, dec3_set_read, dec4_set_read, dec5_set_read, dec6_set_read, dec7_set_read;
  wire        load0, load1, load2, load3, load4, load5, load6, load7;
  wire        load0_set, load1_set, load2_set, load3_set, load4_set, load5_set, load6_set, load7_set;
  wire        load0_set_read, load1_set_read, load2_set_read, load3_set_read, load4_set_read,
  			  load5_set_read, load6_set_read, load7_set_read;  
  wire[2:0]   val0, val1, val2, val3, val4, val5, val6, val7;
  wire[2:0]   selectedCounterVal;
  wire        notHit;
  wire        hit0, hit1, hit2, hit3;
  wire        miss0, miss1, miss2, miss3;
  

  
  mux2to1_3bits mux1(
                      outIndex,   //When hit = 0
                      lineIndex,  //When hit = 1
                      hit,        //Selector
                      lineOrOutIndex //Output
                    );
                    
  decoder3to8 d1(
                    lineOrOutIndex, //Value to decode
                    decoderOut      //Output
                  );                    
                  
  counter c0(
                clk, 
                reset, 
                dec0_set_read, 
                load0_set_read, 
                val0
              );  
              
  counter c1(
                clk, 
                reset, 
                dec1_set_read, 
                load1_set_read, 
                val1
              );                                
                
                
  counter c2(
                clk, 
                reset, 
                dec2_set_read, 
                load2_set_read, 
                val2
              );
              
              
  counter c3(
                clk, 
                reset, 
                dec3_set_read, 
                load3_set_read, 
                val3
              );

  counter c4(
                clk, 
                reset, 
                dec4_set_read, 
                load4_set_read, 
                val4
              );
  
  counter c5(
                clk, 
                reset, 
                dec5_set_read, 
                load5_set_read, 
                val5
              );  
  
  counter c6(
                clk, 
                reset, 
                dec6_set_read, 
                load6_set_read, 
                val6
              );
              
  counter c7(
                clk, 
                reset, 
                dec7_set_read, 
                load7_set_read, 
                val7
              );            
  mux8to1_3bits mux2(
                      val0,
                      val1,
                      val2,
                      val3,
                      val4,
                      val5,
                      val6,
                      val7,
                      lineOrOutIndex,
                      selectedCounterVal
                    );
   
  comparator_lessthan_3bit comp0(selectedCounterVal, val0, dec0);
  comparator_lessthan_3bit comp1(selectedCounterVal, val1, dec1);   
  comparator_lessthan_3bit comp2(selectedCounterVal, val2, dec2);
  comparator_lessthan_3bit comp3(selectedCounterVal, val3, dec3);
  comparator_lessthan_3bit comp4(selectedCounterVal, val4, dec4);
  comparator_lessthan_3bit comp5(selectedCounterVal, val5, dec5);
  comparator_lessthan_3bit comp6(selectedCounterVal, val6, dec6);
  comparator_lessthan_3bit comp7(selectedCounterVal, val7, dec7);

  
  and a0(load0_set_read, decoderOut[0], setInUse, CPU);
  and a1(load1_set_read, decoderOut[1], setInUse, CPU);
  and a2(load2_set_read, decoderOut[2], setInUse, CPU);
  and a3(load3_set_read, decoderOut[3], setInUse, CPU);
  and a4(load4_set_read, decoderOut[4], setInUse, CPU);
  and a5(load5_set_read, decoderOut[5], setInUse, CPU);
  and a6(load6_set_read, decoderOut[6], setInUse, CPU);
  and a7(load7_set_read, decoderOut[7], setInUse, CPU);
  
  and a8(dec0_set_read, dec0, setInUse, CPU);
  and a9(dec1_set_read, dec1, setInUse, CPU);
  and a10(dec2_set_read, dec2, setInUse, CPU);
  and a11(dec3_set_read, dec3, setInUse, CPU);
  and a12(dec4_set_read, dec4, setInUse, CPU);
  and a13(dec5_set_read, dec5, setInUse, CPU);
  and a14(dec6_set_read, dec6, setInUse, CPU);
  and a15(dec7_set_read, dec7, setInUse, CPU);
  
  priority_encoder pe1(val0,  val1, val2, val3, val4, val5, val6, val7, outIndex);
  
endmodule        

module PlacementAndReplacementCircuit(
                                        input               clk,
                                        input               reset,
                                        input               setInUse,
                                        input               hit,
                                        input[2:0]          lineIndex,
                                        input               valid0,
                                        input               valid1,
                                        input               valid2,
                                        input               valid3,
                                        input               valid4,
                                        input               valid5,
                                        input               valid6,
                                        input               valid7,
                                        input               CPU,
                                        output[2:0]         outIndex
                                      );
                                      
  wire invalidExists;
  wire[2:0] invalidIndex;
  wire[2:0] replacementIndex;
  
  LRU_Counter_Replacement RC(clk, reset, lineIndex, setInUse, hit, CPU, replacementIndex);
  Placement_Circuit PC(valid0, valid1, valid2, valid3, valid4, valid5, valid6, valid7,invalidExists,invalidIndex);
  mux2to1_3bits mux1(replacementIndex, invalidIndex, invalidExists, outIndex);  //Choose invalid index if there is any invalid bit
                                    //Else choose replacement index                                      
endmodule


module Tag_Register_Cache(
                                input                 clk,
                                input                 reset,
                                input                 writeSignal,
								input[25:0]			  init,
                                input[25:0]           writeData,
                                output[25:0]           val
                              );
         
  D_FF_Cache_withInit d0(clk, reset, writeSignal, init[0], writeData[0], val[0]);                              
  D_FF_Cache_withInit d1(clk, reset, writeSignal, init[1], writeData[1], val[1]);
  D_FF_Cache_withInit d2(clk, reset, writeSignal, init[2], writeData[2], val[2]);
  D_FF_Cache_withInit d3(clk, reset, writeSignal, init[3], writeData[3], val[3]);
  D_FF_Cache_withInit d4(clk, reset, writeSignal, init[4], writeData[4], val[4]);
  D_FF_Cache_withInit d5(clk, reset, writeSignal, init[5], writeData[5], val[5]);
  D_FF_Cache_withInit d6(clk, reset, writeSignal, init[6], writeData[6], val[6]);
  D_FF_Cache_withInit d7(clk, reset, writeSignal, init[7], writeData[7], val[7]);                            
  D_FF_Cache_withInit d8(clk, reset, writeSignal, init[8], writeData[8], val[8]);                              
  D_FF_Cache_withInit d9(clk, reset, writeSignal, init[9], writeData[9], val[9]);
  D_FF_Cache_withInit d10(clk, reset, writeSignal, init[10], writeData[10], val[10]);
  D_FF_Cache_withInit d11(clk, reset, writeSignal, init[11], writeData[11], val[11]);                              
  D_FF_Cache_withInit d12(clk, reset, writeSignal, init[12], writeData[12], val[12]);
  D_FF_Cache_withInit d13(clk, reset, writeSignal, init[13], writeData[13], val[13]);
  D_FF_Cache_withInit d14(clk, reset, writeSignal, init[14], writeData[14], val[14]);
  D_FF_Cache_withInit d15(clk, reset, writeSignal, init[15], writeData[15], val[15]);                              
  D_FF_Cache_withInit d16(clk, reset, writeSignal, init[16], writeData[16], val[16]);
  D_FF_Cache_withInit d17(clk, reset, writeSignal, init[17], writeData[17], val[17]);
  D_FF_Cache_withInit d18(clk, reset, writeSignal, init[18], writeData[18], val[18]);
  D_FF_Cache_withInit d19(clk, reset, writeSignal, init[19], writeData[19], val[19]);
  D_FF_Cache_withInit d20(clk, reset, writeSignal, init[20], writeData[20], val[20]);
  D_FF_Cache_withInit d21(clk, reset, writeSignal, init[21], writeData[21], val[21]);
  D_FF_Cache_withInit d22(clk, reset, writeSignal, init[22], writeData[22], val[22]);
  D_FF_Cache_withInit d23(clk, reset, writeSignal, init[23], writeData[23], val[23]);
  D_FF_Cache_withInit d24(clk, reset, writeSignal, init[24], writeData[24], val[24]);
  D_FF_Cache_withInit d25(clk, reset, writeSignal, init[25], writeData[25], val[25]);
  
  
endmodule    

module Word_Reader_Cache(
                    input[127:0]                block,
                    input[1:0]                  index,
                    output[31:0]                 word
                  );
//Accepts a block and gives the required byte determined by index  
  mux4to1_32bits mux1( block[31:0],
                      block[63:32],
                      block[95:64],
                      block[127:96],               
                      index,
                      word);                    
                  
endmodule  

module Word_Register_Cache( 
                            input             clk,
                            input             reset,
                            input             writeSignal,
							input[31:0]		  init,
                            input[31:0]        writeData,
                            output [31:0]       val
                          );
                          
  D_FF_Cache_withInit  d0( clk,  reset,  writeSignal, init[0],  writeData[0],  val[0]);
  D_FF_Cache_withInit  d1( clk,  reset,  writeSignal, init[1], writeData[1],  val[1]);
  D_FF_Cache_withInit  d2( clk,  reset,  writeSignal, init[2], writeData[2],  val[2]);
  D_FF_Cache_withInit  d3( clk,  reset,  writeSignal, init[3], writeData[3],  val[3]);
  D_FF_Cache_withInit  d4( clk,  reset,  writeSignal, init[4], writeData[4],  val[4]);
  D_FF_Cache_withInit  d5( clk,  reset,  writeSignal, init[5], writeData[5],  val[5]);
  D_FF_Cache_withInit  d6( clk,  reset,  writeSignal, init[6], writeData[6],  val[6]);
  D_FF_Cache_withInit  d7( clk,  reset,  writeSignal, init[7], writeData[7],  val[7]);
  D_FF_Cache_withInit  d8( clk,  reset,  writeSignal, init[8], writeData[8],  val[8]);
  D_FF_Cache_withInit  d9( clk,  reset,  writeSignal,  init[9],writeData[9],  val[9]);
  D_FF_Cache_withInit  d10( clk,  reset,  writeSignal, init[10], writeData[10],  val[10]);
  D_FF_Cache_withInit  d11( clk,  reset,  writeSignal, init[11], writeData[11],  val[11]);
  D_FF_Cache_withInit  d12( clk,  reset,  writeSignal, init[12], writeData[12],  val[12]);
  D_FF_Cache_withInit  d13( clk,  reset,  writeSignal, init[13], writeData[13],  val[13]);
  D_FF_Cache_withInit  d14( clk,  reset,  writeSignal, init[14], writeData[14],  val[14]);
  D_FF_Cache_withInit  d15( clk,  reset,  writeSignal, init[15], writeData[15],  val[15]);
  D_FF_Cache_withInit  d16( clk,  reset,  writeSignal, init[16], writeData[16],  val[16]);
  D_FF_Cache_withInit  d17( clk,  reset,  writeSignal, init[17],  writeData[17],  val[17]);
  D_FF_Cache_withInit  d18( clk,  reset,  writeSignal, init[18], writeData[18],  val[18]);
  D_FF_Cache_withInit  d19( clk,  reset,  writeSignal, init[19], writeData[19],  val[19]);
  D_FF_Cache_withInit  d20( clk,  reset,  writeSignal, init[20],  writeData[20],  val[20]);
  D_FF_Cache_withInit  d21( clk,  reset,  writeSignal, init[21], writeData[21],  val[21]);
  D_FF_Cache_withInit  d22( clk,  reset,  writeSignal, init[22], writeData[22],  val[22]);
  D_FF_Cache_withInit  d23( clk,  reset,  writeSignal, init[23], writeData[23],  val[23]);
  D_FF_Cache_withInit  d24( clk,  reset,  writeSignal, init[24], writeData[24],  val[24]);
  D_FF_Cache_withInit  d25( clk,  reset,  writeSignal, init[25], writeData[25],  val[25]);
  D_FF_Cache_withInit  d26( clk,  reset,  writeSignal, init[26], writeData[26],  val[26]);
  D_FF_Cache_withInit  d27( clk,  reset,  writeSignal, init[27], writeData[27],  val[27]);
  D_FF_Cache_withInit  d28( clk,  reset,  writeSignal, init[28], writeData[28],  val[28]);
  D_FF_Cache_withInit  d29( clk,  reset,  writeSignal, init[29], writeData[29],  val[29]);
  D_FF_Cache_withInit  d30( clk,  reset,  writeSignal, init[30], writeData[30],  val[30]);
  D_FF_Cache_withInit  d31( clk,  reset,  writeSignal, init[31], writeData[31],  val[31]);
                             
endmodule                          

/*module block_tag_valid(
						input clk, 
						input reset, 
						input[3:0] writeSignal, 
						input[127:0] writeData, 
						input[25:0] writeTag, 
						input valid, 
						output[127:0] blockData, 
						output[25:0] tag, 
						output validOut
						);

wire write;
Block_Cache_4Word b0(
					clk,
					reset,
					writeSignal, 
					writeData, 
					blockData);
or or1(write, writeSignal[0], writeSignal[1], writeSignal[2], writeSignal[3]);
Tag_Register_Cache t0(
						clk, 
						reset, 
						write, 
						writeTag, 
						tag);
valid_Register_Cache v0(
						clk, 
						reset, 
						write, 
						valid, 
						validOut);

endmodule*/

module comparator(input[31:0] PA, 
				  input[25:0] tag0, 
				  input[25:0] tag1, 
				  input[25:0] tag2, 
				  input[25:0] tag3,
				  input[25:0] tag4, 
				  input[25:0] tag5, 
				  input[25:0] tag6, 
				  input[25:0] tag7, 
				  input[7:0] valid,
				  output[7:0] comparator);

reg[7:0] tagOut;				 
always @( PA,tag0,tag1,tag2,tag3,tag4,tag5,tag6,tag7,valid)
	begin
		tagOut=8'b00000000;
		if(PA[31:6]==tag0) tagOut[0]=1;
		else if(PA[31:6]==tag1)	tagOut[1]=1;
		else if(PA[31:6]==tag2)	tagOut[2]=1;
		else if(PA[31:6]==tag3)	tagOut[3]=1;
		else if(PA[31:6]==tag4)	tagOut[4]=1;
		else if(PA[31:6]==tag5)	tagOut[5]=1;
		else if(PA[31:6]==tag6)	tagOut[6]=1;
		else if(PA[31:6]==tag7)	tagOut[7]=1;
	end

	and(comparator[0],tagOut[0],valid[0]);
	and(comparator[1],tagOut[1],valid[1]);
	and(comparator[2],tagOut[2],valid[2]);
	and(comparator[3],tagOut[3],valid[3]);
	and(comparator[4],tagOut[4],valid[4]);
	and(comparator[5],tagOut[5],valid[5]);
	and(comparator[6],tagOut[6],valid[6]);
	and(comparator[7],tagOut[7],valid[7]);
		
endmodule


module comparator_lessthan_3bit(  
                                  input[2:0]        in1,
                                  input[2:0]        in2,
                                  output reg        out
                                );
                 
  //A 3-bit comparator that returns 1 if the first input is strictly less than the second,
  //otherwise 0
  
  //Used in LRU counter replacement strategy implementation
  
  //  in1 - The counter at line index or the out index
  //  in2 - The counter value from a specific counter                 
                                
  always @(in1 or in2)
  begin
    if(in1 < in2)
        out = 1'b1;
    else
        out = 1'b0;
  end         
                                
endmodule

module decoder2to4(input[1:0] in, 
					output reg[3:0] out);
	always@(in)
	case(in)
		2'b00: out = 4'b0001;
		2'b01: out = 4'b0010;
		2'b10: out = 4'b0100;
		2'b11: out = 4'b1000;
	endcase
endmodule	 


module decoder3to8(
                    input[2:0]          in,
                    output reg[7:0]     out
                  );
         
always @(in)
begin
  case(in)
    0:  out = 8'b00000001;
    1:  out = 8'b00000010;
    2:  out = 8'b00000100;
    3:  out = 8'b00001000;
    4:  out = 8'b00010000;
    5:  out = 8'b00100000;
    6:  out = 8'b01000000;
    7:  out = 8'b10000000;
  endcase
end                  
                  
endmodule  

                
module decoder4to16( 
                      input[3:0]                in,
                      output reg[15:0]          out
                    );
         
  always @(in)
  begin
    case(in)
      0:        out = 16'b0000000000000001;
      1:        out = 16'b0000000000000010;
      2:        out = 16'b0000000000000100;
      3:        out = 16'b0000000000001000;
      4:        out = 16'b0000000000010000;
      5:        out = 16'b0000000000100000;
      6:        out = 16'b0000000001000000;
      7:        out = 16'b0000000010000000;
      8:        out = 16'b0000000100000000;
      9:        out = 16'b0000001000000000;
      10:       out = 16'b0000010000000000;
      11:       out = 16'b0000100000000000;
      12:       out = 16'b0001000000000000;
      13:       out = 16'b0010000000000000;
      14:       out = 16'b0100000000000000;
      15:       out = 16'b1000000000000000;

    endcase
  end         
                    
endmodule 

module encoder8to3(
                    input[7:0]      in,
                    output reg[2:0] out,
					output reg 		hit
                  );

always @(in)
begin
  case(in)
    8'b00000001:    begin out = 3'b000; hit=1; end
    8'b00000010:    begin out = 3'b001; hit=1; end
    8'b00000100:    begin out = 3'b010; hit=1; end
    8'b00001000:    begin out = 3'b011; hit=1; end
    8'b00010000:    begin out = 3'b100; hit=1; end
    8'b00100000:    begin out = 3'b101; hit=1; end
    8'b01000000:    begin out = 3'b110; hit=1; end
    8'b10000000:    begin out = 3'b111; hit=1; end
    8'b00000000:    begin out = 3'b000; hit=0; end  //Miss case
  endcase
  
end
                  
endmodule 

module mux2to1_32bits(  
                      input[31:0]              in1,
                      input[31:0]              in2,
                      input                   sel,
                      output reg[31:0]         out
                    );
                    
always @(in1 or in2 or sel)
begin
  if(sel == 0)
    begin
      out = in1;
    end
  else
    begin
      out = in2;
    end
end
                    
endmodule   

module mux2to1_2bits(  
                      input[1:0]              in1,
                      input[1:0]              in2,
                      input                   sel,
                      output reg[1:0]         out
                    );
                    
always @(in1 or in2 or sel)
begin
  if(sel == 0)
    begin
      out = in1;
    end
  else
    begin
      out = in2;
    end
end
                    
endmodule   

module mux2to1_1bit(  
                      input           	 in1,
                      input            	 in2,
                      input              sel,
                      output reg         out
                    );
                    
always @(in1 or in2 or sel)
begin
  if(sel == 0)
    begin
      out = in1;
    end
  else
    begin
      out = in2;
    end
end
                    
endmodule   
                 
module mux2to1_3bits(  
                      input[2:0]              in1,
                      input[2:0]              in2,
                      input                   sel,
                      output reg[2:0]         out
                    );
                    
always @(in1 or in2 or sel)
begin
  if(sel == 0)		out = in1;
  else      out = in2;
end
                    
endmodule  

module mux2to1_4bits(  
                      input[3:0]              in1,
                      input[3:0]              in2,
                      input                   sel,
                      output reg[3:0]         out
                    );
                    
always @(in1 or in2 or sel)
begin
  if(sel == 0)		out = in1;
  else      out = in2;
end
                    
endmodule  

                 
module mux4to1_128bits(  
                        input[127:0]                in1,
                        input[127:0]                in2,
                        input[127:0]                in3,
                        input[127:0]                in4,
                        input[1:0]                  sel,
                        output reg[127:0]           out
                      );
  //Used to select between the outputs from the 4 different blocks
  //Selector is the encoder output
                      
  always @(in1, in2, in3, in4, sel)
  begin
    case(sel)
      0:  out = in1;
      1:  out = in2;
      2:  out = in3;
      3:  out = in4;
    endcase
  end                      
                    
                      
endmodule                    


module mux4to1_26bits(  
                        input[25:0]                in1,
                        input[25:0]                in2,
                        input[25:0]                in3,
                        input[25:0]                in4,
                        input[1:0]                  sel,
                        output reg[25:0]           out
                      );
  //Used to select between the outputs from the 4 different tags
  //Selector is the encoder output
                      
  always @(in1, in2, in3, in4, sel)
  begin
    case(sel)
      0:  out = in1;
      1:  out = in2;
      2:  out = in3;
      3:  out = in4;
    endcase
  end                      
                    
                      
endmodule                    

module mux4to1_32bits(  
                        input[31:0]                in1,
                        input[31:0]                in2,
                        input[31:0]                in3,
                        input[31:0]                in4,
                        input[1:0]                  sel,
                        output reg[31:0]           out
                      );
  //Used to select between the outputs from the 4 different tags
  //Selector is the encoder output
                      
  always @(in1, in2, in3, in4, sel)
  begin
    case(sel)
      0:  out = in1;
      1:  out = in2;
      2:  out = in3;
      3:  out = in4;
    endcase
  end                      
                    
                      
endmodule                    

module mux4to1_3bits(  
                        input[2:0]                in1,
                        input[2:0]                in2,
                        input[2:0]                in3,
                        input[2:0]                in4,
                        input[1:0]                  sel,
                        output reg[2:0]           out
                      );
  //Used to select between the outputs from the 4 LRU counters
  //Selector is the Index PA[5:4]
                      
  always @(in1, in2, in3, in4, sel)
  begin
    case(sel)
      0:  out = in1;
      1:  out = in2;
      2:  out = in3;
      3:  out = in4;
    endcase
  end                      
                      
endmodule                    


module mux4to1_1bit(  
                        input               in1,
                        input                in2,
                        input                in3,
                        input                in4,
                        input[1:0]                  sel,
                        output reg           out
                      );
  //Used to select between the outputs from the 4 LRU counters
  //Selector is the Index PA[5:4]
                      
  always @(in1, in2, in3, in4, sel)
  begin
    case(sel)
      0:  out = in1;
      1:  out = in2;
      2:  out = in3;
      3:  out = in4;
    endcase
  end                      
                      
endmodule

module mux8to1_128bits(  
                        input[127:0]                in1,
                        input[127:0]                in2,
                        input[127:0]                in3,
                        input[127:0]                in4,
                        input[127:0]                in5,
                        input[127:0]                in6,
                        input[127:0]                in7,
                        input[127:0]                in8,
                        input[2:0]                  sel,
                        output reg[127:0]           out
                      );
  //Used to select between the outputs from the 4 different ways
  //Selector is the encoder output
                      
  always @(in1, in2, in3, in4, in5, in6, in7, in8, sel)
  begin
    case(sel)
      0:  out = in1;
      1:  out = in2;
      2:  out = in3;
      3:  out = in4;
      4:  out = in5;
      5:  out = in6;
      6:  out = in7;
      7:  out = in8;
    endcase
  end                      
                    
                      
endmodule                    

module mux8to1_3bits(  
                        input[2:0]                in1,
                        input[2:0]                in2,
                        input[2:0]                in3,
                        input[2:0]                in4,
                        input[2:0]                in5,
                        input[2:0]                in6,
                        input[2:0]                in7,
                        input[2:0]                in8,
                        input[2:0]                  sel,
                        output reg[2:0]           out
                      );
 
  //Selector is the encoder output
                      
  always @(in1, in2, in3, in4, in5, in6, in7, in8, sel)
  begin
    case(sel)
      0:  out = in1;
      1:  out = in2;
      2:  out = in3;
      3:  out = in4;
      4:  out = in5;
      5:  out = in6;
      6:  out = in7;
      7:  out = in8;
    endcase
  end                      
                    
                      
endmodule                    


module Placement_Circuit( 
                          input                   valid0,
                          input                   valid1,
                          input                   valid2,
                          input                   valid3,
                          input					  valid4,
                          input					  valid5,
                          input					  valid6,
                          input					  valid7,
                          output                  anyInvalid,
                          output[2:0]             invalidIndex
                        );
  //Gives write index for a specific set
  //Will need 4 of these in top module
  
  nand n1(anyInvalid, valid0, valid1, valid2, valid3, valid4, valid5, valid6, valid7);    //If any is zero, then output is 1         
  priority_Encoder_Invalid pe1(valid0, valid1, valid2, valid3, valid4, valid5,
  								 valid6, valid7, invalidIndex);
  
endmodule 


module priority_encoder(  
                          input[2:0]      in1,
                          input[2:0]      in2,
                          input[2:0]      in3,
                          input[2:0]      in4,
                          input[2:0]      in5,
                          input[2:0]      in6,
                          input[2:0]      in7,
                          input[2:0]      in8,
                          output reg[2:0] out
                        );                        
  //Takes 8 3-bit inputs and returns the index of the first one
  //which is zero
  
  //Used in the LRU counter implementation to find the line index
  //where replacement is to take place                        
         
  always @(in1, in2, in3, in4, in5, in6, in7, in8)
  begin
    if(in1 == 3'b000) 	out = 3'b000;
    else if(in2 == 3'b000)  	out = 3'b001;
    else if(in3 == 3'b000)	 	out = 3'b010;
    else if(in4 == 3'b000)  	out = 3'b011;      
    else if(in5 == 3'b000)  	out = 3'b100;
    else if(in6 == 3'b000)	 	out = 3'b101;
    else if(in7 == 3'b000)  	out = 3'b110;
    else	out = 3'b111;			//If first 7 aren't zero, the last one must be
  end                        
                        
endmodule  


module priority_Encoder_Invalid(                                
                                input                 valid0,
                                input                 valid1,
                                input                 valid2,
                                input                 valid3,
                                input                 valid4,
                                input                 valid5,
                                input                 valid6,
                                input                 valid7,
                                output  reg[2:0]      invalidIndex
                              );
                              
  always @(valid0, valid1, valid2, valid3, valid4, valid5, valid6, valid7)
  begin
    if(valid0 == 0) invalidIndex = 3'b000;
    else if(valid1 == 0) invalidIndex = 3'b001;
    else if(valid2 == 0) invalidIndex = 3'b010;
    else if(valid3 == 0) invalidIndex = 3'b011;
    else if(valid4 == 0) invalidIndex = 3'b100;
    else if(valid5 == 0) invalidIndex = 3'b101;
    else if(valid6 == 0) invalidIndex = 3'b110;
    else if(valid7 == 0) invalidIndex = 3'b111;
    else invalidIndex = 3'b000; 
  end                              
                              
endmodule 

module valid_Register_Cache(
                                input                 clk,
                                input                 reset,
                                input                 writeSignal,
                                input          		  writeData,
                                output           	  val
                            );
         
  D_FF_Cache d0(clk, reset, writeSignal, writeData, val);
endmodule

module cache_maybe_DM(
								input 				clk, 
								input 				reset, 
								input[31:0] 		PA, 
								input				memAvailable,
								input[127:0] 		memoryBlock,
								input[31:0] 		CPUOut,
								input               CPURead,
								input               CPUWrite,
								output              hit,
								output[31:0]         cacheData
                  );
    wire[3:0] dataWrite;
    wire[2:0] blockSelect, outIndex0,outIndex1,outIndex2,outIndex3,placeOrReplaceIndex, wayIndex;                           
    wire[3:0] setDecoderOut,
			  writeBlock0,writeBlock1,writeBlock2,writeBlock3,writeBlock4,writeBlock5,writeBlock6,writeBlock7;//for Tag and Valid   
    wire[3:0] validOut0,validOut1,validOut2,validOut3,validOut4,validOut5,validOut6,validOut7;
    wire[7:0] valid, comparator, wayIndexDecoderOut,replaceAndSelect;
    wire[127:0] val00,val10,val20,val30,val01,val11,val21,val31,val02,val12,val22,val32,val03,val13,val23,val33,
    			val04,val14,val24,val34,val05,val15,val25,val35,val06,val16,val26,val36,val07,val17,val27,val37,
    			block0,block1,block2,block3,block4,block5,block6,block7,blockOut, writeData;
    wire[25:0] tag00,tag10,tag20,tag30,tag01,tag11,tag21,tag31,tag02,tag12,tag22,tag32,tag03,tag13,tag23,tag33,
    		   tag04,tag14,tag24,tag34,tag05,tag15,tag25,tag35,tag06,tag16,tag26,tag36,tag07,tag17,tag27,tag37,
    		   tagOut0,tagOut1,tagOut2,tagOut3,tagOut4,tagOut5,tagOut6,tagOut7;           

	wire miss,CPU,gugu,lulu, replace, replaceWay0,replaceWay1,replaceWay2,replaceWay3,
			replaceWay4,replaceWay5,replaceWay6,replaceWay7,
			CPUWay0,CPUWay1,CPUWay2,CPUWay3,CPUWay4,CPUWay5,CPUWay6,CPUWay7;
			
	not n0(miss, hit);
	or o0(CPU, CPURead, CPUWrite);
	
	mux2to1_3bits wayDecoderInputMux( placeOrReplaceIndex, blockSelect, hit, wayIndex);
	decoder3to8 wayDecoder(wayIndex, wayIndexDecoderOut);    
	decoder2to4 setDecoder(PA[5:4], setDecoderOut);
	wire [3:0] offsetDecoderOut;
	decoder2to4 offsetDecoder(PA[3:2], offsetDecoderOut);
	
	
	and(replaceWay0,replace,wayIndexDecoderOut[0]);
  	and(replaceWay1,replace,wayIndexDecoderOut[1]);
  	and(replaceWay2,replace,wayIndexDecoderOut[2]);
  	and(replaceWay3,replace,wayIndexDecoderOut[3]);
    and(replaceWay4,replace,wayIndexDecoderOut[4]);
    and(replaceWay5,replace,wayIndexDecoderOut[5]);
    and(replaceWay6,replace,wayIndexDecoderOut[6]);
    and(replaceWay7,replace,wayIndexDecoderOut[7]);
    
    and(CPUWay0,WriteFinal,wayIndexDecoderOut[0]);
  	and(CPUWay1,WriteFinal,wayIndexDecoderOut[1]);
  	and(CPUWay2,WriteFinal,wayIndexDecoderOut[2]);
  	and(CPUWay3,WriteFinal,wayIndexDecoderOut[3]);
  	and(CPUWay4,WriteFinal,wayIndexDecoderOut[4]);
  	and(CPUWay5,WriteFinal,wayIndexDecoderOut[5]);
  	and(CPUWay6,WriteFinal,wayIndexDecoderOut[6]);
  	and(CPUWay7,WriteFinal,wayIndexDecoderOut[7]);
  
	and(replaceAndSelect[0], wayIndexDecoderOut[0],replace);
	and(replaceAndSelect[1], wayIndexDecoderOut[1],replace);
	and(replaceAndSelect[2], wayIndexDecoderOut[2],replace);
	and(replaceAndSelect[3], wayIndexDecoderOut[3],replace);
	and(replaceAndSelect[4], wayIndexDecoderOut[4],replace);
	and(replaceAndSelect[5], wayIndexDecoderOut[5],replace);
	and(replaceAndSelect[6], wayIndexDecoderOut[6],replace);
	and(replaceAndSelect[7], wayIndexDecoderOut[7],replace);
	
	And_4_Array ws0(replaceAndSelect[0],setDecoderOut,writeBlock0);
	And_4_Array ws1(replaceAndSelect[1],setDecoderOut,writeBlock1);
	And_4_Array ws2(replaceAndSelect[2],setDecoderOut,writeBlock2);
	And_4_Array ws3(replaceAndSelect[3],setDecoderOut,writeBlock3);
	And_4_Array ws4(replaceAndSelect[4],setDecoderOut,writeBlock4);
	And_4_Array ws5(replaceAndSelect[5],setDecoderOut,writeBlock5);
	And_4_Array ws6(replaceAndSelect[6],setDecoderOut,writeBlock6);
	And_4_Array ws7(replaceAndSelect[7],setDecoderOut,writeBlock7);
  
  //mux to decide when CPU should Write
  and aCpuWrRepl(WrReplace, CPUWrite,replace);
  mux2to1_1bit whenCpuWr(CPUWrite,  WrReplace, replace,  WriteFinal);
  
                                    
	Data_Array d0(clk,reset,CPUWay0,replace,PA[3:0],PA[5:4], 128'h00000003_00000002_00000001_00000000,128'h00000007_00000006_00000005_00000004,128'h0,0 ,writeData,CPUOut,
				  val00,val10,val20,val30);																																
	Tag_Array t0(clk,reset,writeBlock0,
					PA[31:6],26'b0,26'b0,0,0,tag00,tag10,tag20,tag30);
	Valid_Array v0(clk,reset,writeBlock0,4'b0011,4'b1111
					,validOut0);
	mux4to1_128bits m00(val00,val10,val20,val30,PA[5:4],block0);
	mux4to1_26bits m01(tag00,tag10,tag20,tag30,PA[5:4],tagOut0);
	mux4to1_1bit m02(validOut0[0],validOut0[1],validOut0[2],validOut0[3],PA[5:4],valid[0]);
	
	Data_Array d1(clk,reset,CPUWay1,replace,PA[3:0],PA[5:4],128'h0,128'h0,0,0,writeData,CPUOut,
				  val01,val11,val21,val31);
	Tag_Array t1(clk,reset,writeBlock1,
					PA[31:6],0,0,0,0,tag01,tag11,tag21,tag31);
	Valid_Array v1(clk,reset,writeBlock1,4'b0000,4'b1111
					,validOut1);
	mux4to1_128bits m10(val01,val11,val21,val31,PA[5:4],block1);
	mux4to1_26bits m11(tag01,tag11,tag21,tag31,PA[5:4],tagOut1);
	mux4to1_1bit m12(validOut1[0],validOut1[1],validOut1[2],validOut1[3],PA[5:4],valid[1]);
	
	Data_Array d2(clk,reset,CPUWay2,replace,PA[3:0],PA[5:4],0,0,0,0,writeData,CPUOut,
				  val02,val12,val22,val32);
	Tag_Array t2(clk,reset,writeBlock2,
					PA[31:6],0,0,0,0,tag02,tag12,tag22,tag32);
	Valid_Array v2(clk,reset,writeBlock2,0,4'b1111
					,validOut2);
	mux4to1_128bits m20(val02,val12,val22,val32,PA[5:4],block2);
	mux4to1_26bits m21(tag02,tag12,tag22,tag32,PA[5:4],tagOut2);
	mux4to1_1bit m22(validOut2[0],validOut2[1],validOut2[2],validOut2[3],PA[5:4],valid[2]);
	
	Data_Array d3(clk,reset,CPUWay3,replace,PA[3:0],PA[5:4],0,0,0,0,writeData,CPUOut,
				  val03,val13,val23,val33);
	Tag_Array t3(clk,reset,writeBlock3,
					PA[31:6],0,0,0,0,tag03,tag13,tag23,tag33);
	Valid_Array v3(clk,reset,writeBlock3,0,4'b1111
					,validOut3);
	mux4to1_128bits m30(val03,val13,val23,val33,PA[5:4],block3);
	mux4to1_26bits m31(tag03,tag13,tag23,tag33,PA[5:4],tagOut3);
	mux4to1_1bit m32(validOut3[0],validOut3[1],validOut3[2],validOut3[3],PA[5:4],valid[3]);
	
	Data_Array d4(clk,reset,CPUWay4,replace,PA[3:0],PA[5:4],0,0,0,0,writeData,CPUOut,
				  val04,val14,val24,val34);
	Tag_Array t4(clk,reset,writeBlock4,
					PA[31:6],0,0,0,0,tag04,tag14,tag24,tag34);
	Valid_Array v4(clk,reset,writeBlock4,0,4'b1111
					,validOut4);
	mux4to1_128bits m40(val04,val14,val24,val34,PA[5:4],block4);
	mux4to1_26bits m41(tag04,tag14,tag24,tag34,PA[5:4],tagOut4);
	mux4to1_1bit m42(validOut4[0],validOut4[1],validOut4[2],validOut4[3],PA[5:4],valid[4]);
	
	Data_Array d5(clk,reset,CPUWay5,replace,PA[3:0],PA[5:4],0,0,0,0,writeData,CPUOut,
				  val05,val15,val25,val35);
	Tag_Array t5(clk,reset,writeBlock5,
					PA[31:6],0,0,0,0,tag05,tag15,tag25,tag35);
	Valid_Array v5(clk,reset,writeBlock5,0,4'b1111
					,validOut5);
	mux4to1_128bits m50(val05,val15,val25,val35,PA[5:4],block5);
	mux4to1_26bits m51(tag05,tag15,tag25,tag35,PA[5:4],tagOut5);
	mux4to1_1bit m52(validOut5[0],validOut5[1],validOut5[2],validOut5[3],PA[5:4],valid[5]);
	
	Data_Array d6(clk,reset,CPUWay6,replace,PA[3:0],PA[5:4],0,0,0,0,writeData,CPUOut,
				  val06,val16,val26,val36);
	Tag_Array t6(clk,reset,writeBlock6,
					PA[31:6],0,0,0,0,tag06,tag16,tag26,tag36);
	Valid_Array v6(clk,reset,writeBlock6,0,4'b1111
					,validOut6);
	mux4to1_128bits m60(val06,val16,val26,val36,PA[5:4],block6);
	mux4to1_26bits m61(tag06,tag16,tag26,tag36,PA[5:4],tagOut6);
	mux4to1_1bit m62(validOut6[0],validOut6[1],validOut6[2],validOut6[3],PA[5:4],valid[6]);
	
	Data_Array d7(clk,reset,CPUWay7,replace,PA[3:0],PA[5:4], 0,0,0,128'h0,writeData,CPUOut,
				  val07,val17,val27,val37);
	Tag_Array t7(clk,reset,writeBlock7,
					PA[31:6],0,0,0,26'b0,tag07,tag17,tag27,tag37);
	Valid_Array v7(clk,reset,writeBlock7,4'b0000,4'b1111
					,validOut7);  			  
	mux4to1_128bits m70(val07,val17,val27,val37,PA[5:4],block7);
	mux4to1_26bits m71(tag07,tag17,tag27,tag37,PA[5:4],tagOut7);
	mux4to1_1bit m72(validOut7[0],validOut7[1],validOut7[2],validOut7[3],PA[5:4],valid[7]);
	
	comparator c0(PA, tagOut0, tagOut1, tagOut2, tagOut3, tagOut4, tagOut5, tagOut6, tagOut7, 
			valid, comparator);
	encoder8to3 e0(comparator, blockSelect,hit);
	
	mux8to1_128bits cache_block_select( block0, block1, block2, block3, block4, block5, 
					block6, block7, blockSelect,blockOut);
	mux4to1_32bits cache_word_select( blockOut[31:0], blockOut[63:32], blockOut[95:64], blockOut[127:96],
					 			 PA[3:2], cacheData);
	
	PlacementAndReplacementCircuit par0(clk,reset,setDecoderOut[0],hit,blockSelect,valid[0],valid[1],
                            valid[2],valid[3],valid[4],valid[5],valid[6],valid[7],CPU,outIndex0);
	PlacementAndReplacementCircuit par1(clk,reset,setDecoderOut[1],hit,blockSelect,valid[0],valid[1],
                            valid[2],valid[3],valid[4],valid[5],valid[6],valid[7],CPU,outIndex1);
	PlacementAndReplacementCircuit par2(clk,reset,setDecoderOut[2],hit,blockSelect,valid[0],valid[1],
                            valid[2],valid[3],valid[4],valid[5],valid[6],valid[7],CPU,outIndex2);
	PlacementAndReplacementCircuit par3(clk,reset,setDecoderOut[3],hit,blockSelect,valid[0],valid[1],
                            valid[2],valid[3],valid[4],valid[5],valid[6],valid[7],CPU,outIndex3);
                            
    mux4to1_3bits PlacementAndReplacementMux(outIndex0, outIndex1,
                                        outIndex2, outIndex3,
                                        PA[5:4], placeOrReplaceIndex);
                              
    wire[3:0] writeToWord;   //Whether we're writing to the specific word or not

	and a0(writeToWord[0], offsetDecoderOut[0], CPUWrite, hit);
	and a1(writeToWord[1], offsetDecoderOut[1], CPUWrite, hit);
	and a2(writeToWord[2], offsetDecoderOut[2], CPUWrite, hit);
	and a3(writeToWord[3], offsetDecoderOut[3], CPUWrite, hit);
	
	mux2to1_32bits WriteData0(memoryBlock[31:0], CPUOut, writeToWord[0], writeData[31:0]);
	mux2to1_32bits WriteData1(memoryBlock[63:32], CPUOut, writeToWord[1], writeData[63:32]);
	mux2to1_32bits WriteData2(memoryBlock[95:64], CPUOut, writeToWord[2], writeData[95:64]);
	mux2to1_32bits WriteData3(memoryBlock[127:96], CPUOut, writeToWord[3], writeData[127:96]); 
	
	and(replace, CPU, miss, memAvailable); //Write into cache from memory on miss and (MemRead or MemWrite) && Memory Available
	
endmodule
module cache_maybe_IM(
								input 				clk, 
								input 				reset, 
								input[31:0] 		PA, 
								input				memAvailable,
								input[127:0] 		memoryBlock,
								input[31:0] 		CPUOut,
								input               CPURead,
								input               CPUWrite,
								output              hit,
								output[31:0]         cacheData
                  );
    wire[3:0] dataWrite;
    wire[2:0] blockSelect, outIndex0,outIndex1,outIndex2,outIndex3,placeOrReplaceIndex, wayIndex;                           
    wire[3:0] setDecoderOut,
			  writeBlock0,writeBlock1,writeBlock2,writeBlock3,writeBlock4,writeBlock5,writeBlock6,writeBlock7;//for Tag and Valid   
    wire[3:0] validOut0,validOut1,validOut2,validOut3,validOut4,validOut5,validOut6,validOut7;
    wire[7:0] valid, comparator, wayIndexDecoderOut,replaceAndSelect;
    wire[127:0] val00,val10,val20,val30,val01,val11,val21,val31,val02,val12,val22,val32,val03,val13,val23,val33,
    			val04,val14,val24,val34,val05,val15,val25,val35,val06,val16,val26,val36,val07,val17,val27,val37,
    			block0,block1,block2,block3,block4,block5,block6,block7,blockOut, writeData;
    wire[25:0] tag00,tag10,tag20,tag30,tag01,tag11,tag21,tag31,tag02,tag12,tag22,tag32,tag03,tag13,tag23,tag33,
    		   tag04,tag14,tag24,tag34,tag05,tag15,tag25,tag35,tag06,tag16,tag26,tag36,tag07,tag17,tag27,tag37,
    		   tagOut0,tagOut1,tagOut2,tagOut3,tagOut4,tagOut5,tagOut6,tagOut7;           

	wire miss,CPU,gugu,lulu, replace, replaceWay0,replaceWay1,replaceWay2,replaceWay3,
			replaceWay4,replaceWay5,replaceWay6,replaceWay7,
			CPUWay0,CPUWay1,CPUWay2,CPUWay3,CPUWay4,CPUWay5,CPUWay6,CPUWay7;
			
	not n0(miss, hit);
	or o0(CPU, CPURead, CPUWrite);
	
	mux2to1_3bits wayDecoderInputMux( placeOrReplaceIndex, blockSelect, hit, wayIndex);
	decoder3to8 wayDecoder(wayIndex, wayIndexDecoderOut);    
	decoder2to4 setDecoder(PA[5:4], setDecoderOut);
	wire [3:0] offsetDecoderOut;
	decoder2to4 offsetDecoder(PA[3:2], offsetDecoderOut);
	
	and(replaceWay0,replace,wayIndexDecoderOut[0]);
  	and(replaceWay1,replace,wayIndexDecoderOut[1]);
  	and(replaceWay2,replace,wayIndexDecoderOut[2]);
  	and(replaceWay3,replace,wayIndexDecoderOut[3]);
    and(replaceWay4,replace,wayIndexDecoderOut[4]);
    and(replaceWay5,replace,wayIndexDecoderOut[5]);
    and(replaceWay6,replace,wayIndexDecoderOut[6]);
    and(replaceWay7,replace,wayIndexDecoderOut[7]);
    
    and(CPUWay0,WriteFinal,wayIndexDecoderOut[0]);
  	and(CPUWay1,WriteFinal,wayIndexDecoderOut[1]);
  	and(CPUWay2,WriteFinal,wayIndexDecoderOut[2]);
  	and(CPUWay3,WriteFinal,wayIndexDecoderOut[3]);
  	and(CPUWay4,WriteFinal,wayIndexDecoderOut[4]);
  	and(CPUWay5,WriteFinal,wayIndexDecoderOut[5]);
  	and(CPUWay6,WriteFinal,wayIndexDecoderOut[6]);
  	and(CPUWay7,WriteFinal,wayIndexDecoderOut[7]);
  
	and(replaceAndSelect[0], wayIndexDecoderOut[0],replace);
	and(replaceAndSelect[1], wayIndexDecoderOut[1],replace);
	and(replaceAndSelect[2], wayIndexDecoderOut[2],replace);
	and(replaceAndSelect[3], wayIndexDecoderOut[3],replace);
	and(replaceAndSelect[4], wayIndexDecoderOut[4],replace);
	and(replaceAndSelect[5], wayIndexDecoderOut[5],replace);
	and(replaceAndSelect[6], wayIndexDecoderOut[6],replace);
	and(replaceAndSelect[7], wayIndexDecoderOut[7],replace);
	
	And_4_Array ws0(replaceAndSelect[0],setDecoderOut,writeBlock0);
	And_4_Array ws1(replaceAndSelect[1],setDecoderOut,writeBlock1);
	And_4_Array ws2(replaceAndSelect[2],setDecoderOut,writeBlock2);
	And_4_Array ws3(replaceAndSelect[3],setDecoderOut,writeBlock3);
	And_4_Array ws4(replaceAndSelect[4],setDecoderOut,writeBlock4);
	And_4_Array ws5(replaceAndSelect[5],setDecoderOut,writeBlock5);
	And_4_Array ws6(replaceAndSelect[6],setDecoderOut,writeBlock6);
	And_4_Array ws7(replaceAndSelect[7],setDecoderOut,writeBlock7);
  
  //mux to decide when CPU should Write
  and aCpuWrRepl(WrReplace, CPUWrite,replace);
  mux2to1_1bit whenCpuWr(CPUWrite,  WrReplace, replace,  WriteFinal);
  
                    //calculate setDecoderOut and wayDecoderOut gaphla                  
	Data_Array d0(clk,reset,CPUWay0,replace,PA[3:0],PA[5:4], 128'h1A0A0000_21020000_20010000_00000000,128'h260C0000_406C0000_25090000_24030000,128'h27080000_00005B8D_000050DA_43700000,128'h00000000_00000000_00000000_0000F140 ,writeData,CPUOut,//baadme dekhna isko
				  val00,val10,val20,val30);																																												//check jump address
	Tag_Array t0(clk,reset,writeBlock0,
					PA[31:6],26'b00000000000000000000000,26'b00000000000000000000000000,26'b00000000000000000000000000,26'b00000000000000000000000000,tag00,tag10,tag20,tag30);
	Valid_Array v0(clk,reset,writeBlock0,4'b1111,4'b1111
					,validOut0);
	mux4to1_128bits m00(val00,val10,val20,val30,PA[5:4],block0);
	mux4to1_26bits m01(tag00,tag10,tag20,tag30,PA[5:4],tagOut0);
	mux4to1_1bit m02(validOut0[0],validOut0[1],validOut0[2],validOut0[3],PA[5:4],valid[0]);
	
	Data_Array d1(clk,reset,CPUWay1,replace,PA[3:0],PA[5:4],128'h1AD15BBD1AD151771A0B502D20045049,128'hFB4D0000_00000000_0000D155_21B50000,128'h1AD15BBD1AD151771A0B502D20045049,128'hFB4D0000_00000000_0000D155_21B50000,writeData,CPUOut,//baadme dekhna isko
				  val01,val11,val21,val31);
	Tag_Array t1(clk,reset,writeBlock1,
					PA[31:6],26'b00000000000000000000001011,26'b00000000000000000000001011,26'b00000000000000000000001011,26'b00000000000000000000001011,tag01,tag11,tag21,tag31);
	Valid_Array v1(clk,reset,writeBlock1,4'b1111,4'b1111
					,validOut1);
	mux4to1_128bits m10(val01,val11,val21,val31,PA[5:4],block1);
	mux4to1_26bits m11(tag01,tag11,tag21,tag31,PA[5:4],tagOut1);
	mux4to1_1bit m12(validOut1[0],validOut1[1],validOut1[2],validOut1[3],PA[5:4],valid[1]);
	
	Data_Array d2(clk,reset,CPUWay2,replace,PA[3:0],PA[5:4],0,0,0,0,writeData,CPUOut,//baadme dekhna isko
				  val02,val12,val22,val32);
	Tag_Array t2(clk,reset,writeBlock2,
					PA[31:6],0,0,0,0,tag02,tag12,tag22,tag32);
	Valid_Array v2(clk,reset,writeBlock2,0,4'b1111
					,validOut2);
	mux4to1_128bits m20(val02,val12,val22,val32,PA[5:4],block2);
	mux4to1_26bits m21(tag02,tag12,tag22,tag32,PA[5:4],tagOut2);
	mux4to1_1bit m22(validOut2[0],validOut2[1],validOut2[2],validOut2[3],PA[5:4],valid[2]);
	
	Data_Array d3(clk,reset,CPUWay3,replace,PA[3:0],PA[5:4],128'h27555A2D_404550DF_1A085B17_20015177,128'h22015177_00000000_0000D155_21B50000,128'h00000000_00000000_00000000_0000D120,0,writeData,CPUOut,//baadme dekhna isko
				  val03,val13,val23,val33);
	Tag_Array t3(clk,reset,writeBlock3,
					PA[31:6],26'b00000000000000000000_001010,26'b00000000000000000000_001010,26'b00000000000000000000_001010,0,tag03,tag13,tag23,tag33);
	Valid_Array v3(clk,reset,writeBlock3,4'b0111,4'b1111
					,validOut3);
	mux4to1_128bits m30(val03,val13,val23,val33,PA[5:4],block3);
	mux4to1_26bits m31(tag03,tag13,tag23,tag33,PA[5:4],tagOut3);
	mux4to1_1bit m32(validOut3[0],validOut3[1],validOut3[2],validOut3[3],PA[5:4],valid[3]);
	
	Data_Array d4(clk,reset,CPUWay4,replace,PA[3:0],PA[5:4],0,0,0,0,writeData,CPUOut,//baadme dekhna isko
				  val04,val14,val24,val34);
	Tag_Array t4(clk,reset,writeBlock4,
					PA[31:6],0,0,0,0,tag04,tag14,tag24,tag34);
	Valid_Array v4(clk,reset,writeBlock4,0,4'b1111
					,validOut4);
	mux4to1_128bits m40(val04,val14,val24,val34,PA[5:4],block4);
	mux4to1_26bits m41(tag04,tag14,tag24,tag34,PA[5:4],tagOut4);
	mux4to1_1bit m42(validOut4[0],validOut4[1],validOut4[2],validOut4[3],PA[5:4],valid[4]);
	
	Data_Array d5(clk,reset,CPUWay5,replace,PA[3:0],PA[5:4],0,0,0,0,writeData,CPUOut,//baadme dekhna isko
				  val05,val15,val25,val35);
	Tag_Array t5(clk,reset,writeBlock5,
					PA[31:6],0,0,0,0,tag05,tag15,tag25,tag35);
	Valid_Array v5(clk,reset,writeBlock5,0,4'b1111
					,validOut5);
	mux4to1_128bits m50(val05,val15,val25,val35,PA[5:4],block5);
	mux4to1_26bits m51(tag05,tag15,tag25,tag35,PA[5:4],tagOut5);
	mux4to1_1bit m52(validOut5[0],validOut5[1],validOut5[2],validOut5[3],PA[5:4],valid[5]);
	
	Data_Array d6(clk,reset,CPUWay6,replace,PA[3:0],PA[5:4],0,0,0,0,writeData,CPUOut,//baadme dekhna isko
				  val06,val16,val26,val36);
	Tag_Array t6(clk,reset,writeBlock6,
					PA[31:6],0,0,0,0,tag06,tag16,tag26,tag36);
	Valid_Array v6(clk,reset,writeBlock6,0,4'b1111
					,validOut6);
	mux4to1_128bits m60(val06,val16,val26,val36,PA[5:4],block6);
	mux4to1_26bits m61(tag06,tag16,tag26,tag36,PA[5:4],tagOut6);
	mux4to1_1bit m62(validOut6[0],validOut6[1],validOut6[2],validOut6[3],PA[5:4],valid[6]);
	
	Data_Array d7(clk,reset,CPUWay7,replace,PA[3:0],PA[5:4], 128'h00000000_00000000_00000000_00000EC9,0,0,128'h00000000_00000000_00000000_00000EC9,writeData,CPUOut,//baadme dekhna isko
				  val07,val17,val27,val37);
	Tag_Array t7(clk,reset,writeBlock7,
					PA[31:6],26'b10000000000000000000000110,0,0,26'b10000000000000000000000110,tag07,tag17,tag27,tag37);
	Valid_Array v7(clk,reset,writeBlock7,4'b1001,4'b1111
					,validOut7);  			  
	mux4to1_128bits m70(val07,val17,val27,val37,PA[5:4],block7);
	mux4to1_26bits m71(tag07,tag17,tag27,tag37,PA[5:4],tagOut7);
	mux4to1_1bit m72(validOut7[0],validOut7[1],validOut7[2],validOut7[3],PA[5:4],valid[7]);
	
	comparator c0(PA, tagOut0, tagOut1, tagOut2, tagOut3, tagOut4, tagOut5, tagOut6, tagOut7, 
			valid, comparator);
	encoder8to3 e0(comparator, blockSelect,hit);
	
	mux8to1_128bits cache_block_select( block0, block1, block2, block3, block4, block5, 
					block6, block7, blockSelect,blockOut);
	mux4to1_32bits cache_word_select( blockOut[31:0], blockOut[63:32], blockOut[95:64], blockOut[127:96],
					 			 PA[3:2], cacheData);
	
	PlacementAndReplacementCircuit par0(clk,reset,setDecoderOut[0],hit,blockSelect,valid[0],valid[1],
                            valid[2],valid[3],valid[4],valid[5],valid[6],valid[7],CPU,outIndex0);
	PlacementAndReplacementCircuit par1(clk,reset,setDecoderOut[1],hit,blockSelect,valid[0],valid[1],
                            valid[2],valid[3],valid[4],valid[5],valid[6],valid[7],CPU,outIndex1);
	PlacementAndReplacementCircuit par2(clk,reset,setDecoderOut[2],hit,blockSelect,valid[0],valid[1],
                            valid[2],valid[3],valid[4],valid[5],valid[6],valid[7],CPU,outIndex2);
	PlacementAndReplacementCircuit par3(clk,reset,setDecoderOut[3],hit,blockSelect,valid[0],valid[1],
                            valid[2],valid[3],valid[4],valid[5],valid[6],valid[7],CPU,outIndex3);
                            
    mux4to1_3bits PlacementAndReplacementMux(outIndex0, outIndex1,
                                        outIndex2, outIndex3,
                                        PA[5:4], placeOrReplaceIndex);
                              
    wire[3:0] writeToWord;   //Whether we're writing to the specific word or not

	and a0(writeToWord[0], offsetDecoderOut[0], CPUWrite, hit);
	and a1(writeToWord[1], offsetDecoderOut[1], CPUWrite, hit);
	and a2(writeToWord[2], offsetDecoderOut[2], CPUWrite, hit);
	and a3(writeToWord[3], offsetDecoderOut[3], CPUWrite, hit);
	
	mux2to1_32bits WriteData0(memoryBlock[31:0], CPUOut, writeToWord[0], writeData[31:0]);
	mux2to1_32bits WriteData1(memoryBlock[63:32], CPUOut, writeToWord[1], writeData[63:32]);
	mux2to1_32bits WriteData2(memoryBlock[95:64], CPUOut, writeToWord[2], writeData[95:64]);
	mux2to1_32bits WriteData3(memoryBlock[127:96], CPUOut, writeToWord[3], writeData[127:96]); 
	
	and(replace, CPU, miss, memAvailable); //Write into cache from memory on miss and (MemRead or MemWrite) && memAvailable
	////Finish the replacement part
endmodule
module TestBench;
  
  reg clk, reset;
  reg[31:0] PA;
  reg[127:0] MemoryBlock;
  reg[31:0] CPUOut;
  reg CPURead, CPUWrite,memAvailable;
  wire hit;
  wire[31:0] cacheData;
  reg[3:0] offset;
  reg[1:0] index;
  reg[25:0] tag;
  

  
  cache_maybe_IM test(
                  clk,
                  reset,
                  PA,
				          memAvailable,
                  MemoryBlock,
                  CPUOut,
                  CPURead,
                  CPUWrite,
                  hit,
                  cacheData
                );  
  
  always
  begin
    #5 clk = ~clk;
  end
  
  always @(PA)
  begin
    offset = PA[3:0];
    index = PA[5:4];
    tag = PA[31:6];
  end
  
  initial
  begin
    
	
	clk = 0; reset = 1; PA = 32'h00000080; memAvailable = 1; MemoryBlock = 64'h0123456789ABCDEF; CPUOut = 32'h20; CPUWrite = 0; CPURead = 1;
    #10 reset = 0; PA = 32'h00000080; memAvailable = 1; MemoryBlock = 64'h0123456789ABCDEF; CPUOut = 32'h20; CPUWrite = 0;CPURead = 1; //Read Miss
	#40 PA = 32'h00000084; CPUOut = 32'h20; CPUWrite = 0; CPURead = 1;	//Read Hit
    #30 PA = 32'h00000100; memAvailable=0; CPUOut = 32'h20; CPUWrite = 0;CPURead = 1;	//Read Miss
	#40 memAvailable=1; MemoryBlock = 64'hFEDCBA9876543210;		//Place, Hit after 1 cycle
	#30 PA = 32'h00000104; CPUOut = 32'h20; CPUWrite = 0; CPURead = 1;	//Read Hit
    #40 PA = 32'h00000190;memAvailable=0; MemoryBlock = 64'hABCDEF0123456789; CPUOut = 32'h20; CPUWrite = 1;CPURead = 0;		//Write Miss
	#40 memAvailable=1;	//Hit after 1 cycle
	#40 PA = 32'h00000194; CPUOut = 32'h20; CPUWrite = 1; CPURead = 0;	//Write Hit
	#40 PA = 32'h00000194; CPUOut = 32'h20; CPUWrite = 0; CPURead = 1;	//Read Hit after writing
    #40 PA = 32'h00000210;memAvailable = 1; MemoryBlock = 64'h9876543210ABCDEF; CPUOut = 32'h20; CPUWrite = 0;CPURead = 1;	//Read Miss(hit after one cycle)
    #40 PA = 32'h00000102;memAvailable = 1; MemoryBlock = 64'h9876543210ABCDEF; CPUOut = 32'h20; CPUWrite = 0;CPURead = 1;	//Read Miss(hit after one cycle)
    #10 PA = 32'h00000102;memAvailable = 1; MemoryBlock = 64'h9876543210ABCDEF; CPUOut = 32'h20; CPUWrite = 1;CPURead = 0;	//Write Hit
    #10 PA = 32'h00000080;memAvailable = 1; MemoryBlock = 64'h9876543210ABCDEF; CPUOut = 32'h20; CPUWrite = 0;CPURead = 1;	//Read Hit
    #20 PA = 32'h00000180;memAvailable = 1; MemoryBlock = 64'hABCDEF9876543210; CPUOut = 32'h20; CPUWrite = 1;CPURead = 0;	//Write Hit
    #10 CPUWrite=0;CPURead = 1;	//Read Hit
    #20 CPUWrite=1;CPURead = 0;	//Write Hit
    #10 CPUWrite = 0;CPURead = 1;	//Read Hit
    #30 $finish;
	
	
  end
  
endmodule

